<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tbl_email_layout
 *
 * @author arif firman syah
 */
class Tbl_email_layout extends MY_Model {

    //put your code here

    public $tableName = 'tbl_email_layout';

    public function _get_all() {
        $email_layout = $this->find('first', array(
            'conditions' => array(
                'is_active' => 1
            )
        ));
        if ($email_layout) {
            $this->load->model('Tbl_email_links');
            $links = $this->Tbl_email_links->find('all', array('conditions' => array('a.is_active' => 1, 'a.email_layout_id' => $email_layout['id'])));
        }
    }

}
