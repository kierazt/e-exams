<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//gobal router
$route['default_controller'] = 'frontend/user';
$route['/'] = 'frontend/user/index';
$route['login'] = 'frontend/user/login';
$route['logout'] = 'frontend/user/logout';
$route['auth-user'] = 'frontend/user/auth';
$route['dashboard'] = 'frontend/user/dashboard';
$route['ticket/(:any)'] = 'frontend/ticket/$1';
$route['ticket/(:any)/(:any)'] = 'frontend/ticket/$1/$2';
$route['my-profile'] = 'frontend/user/my_profile';
$route['my-inbox'] = 'frontend/user/my_inbox';
$route['my-task'] = 'frontend/user/my_task';
$route['my-notif'] = 'frontend/user/my_notif';
$route['lock-screen'] = 'frontend/user/lock_screen';
$route['unlock-screen'] = 'frontend/user/un_lock_screen';
$route['switch-lang'] = 'frontend/user/switch_lang';

//backend route
$route['backend'] = 'backend/settings/user/login';
$route['backend/auth-user'] = 'backend/settings/user/auth';
$route['backend/login'] = 'backend/settings/user/login';
$route['backend/logout'] = 'backend/settings/user/logout';
$route['backend/dashboard'] = 'backend/settings/user/dashboard';
$route['backend/my-profile'] = 'backend/profiles/user/my_profile';
$route['backend/my-inbox'] = 'backend/profiles/user/my_inbox';
$route['backend/my-task'] = 'backend/profiles/user/my_task';
$route['backend/lock-screen'] = 'backend/profiles/user/lock_screen';
$route['backend/unlock-screen'] = 'backend/profiles/user/un_lock_screen';

//lecturer route
$route['lecturer'] = 'lecturer/user/login';
$route['lecturer/auth-user'] = 'lecturer/user/auth';
$route['lecturer/login'] = 'lecturer/user/login';
$route['lecturer/logout'] = 'lecturer/user/logout';
$route['lecturer/dashboard'] = 'lecturer/user/dashboard';
$route['lecturer/my-profile'] = 'lecturer/user/my_profile';
$route['lecturer/my-inbox'] = 'lecturer/user/my_inbox';
$route['lecturer/my-task'] = 'lecturer/user/my_task';
$route['lecturer/lock-screen'] = 'lecturer/user/lock_screen';
$route['lecturer/unlock-screen'] = 'lecturer/user/un_lock_screen';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
