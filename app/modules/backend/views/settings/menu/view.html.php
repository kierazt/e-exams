<style>
    .treeview .list-group-item{cursor:pointer}.treeview span.indent{margin-left:10px;margin-right:10px}.treeview span.icon{width:12px;margin-right:5px}.treeview .node-disabled{color:silver;cursor:not-allowed}.node-treeview12{}.node-treeview12:not(.node-disabled):hover{background-color:#F5F5F5;} 
</style>
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">{view-header-title}</span>
                    <small>logged in required</small>
                </div>
                <div class="actions" style="display:none">
                    <div class="btn-group btn-group-devided" data-toggle="buttons">
                        <div class="btn btn-transparent blue btn-outline btn-circle btn-sm active" data-value="add" id="opt_add">
                            Add
                        </div>
                        <div class="btn btn-transparent green btn-outline btn-circle btn-sm disabled" data-value="edit" id="opt_edit" disabled="">
                            Edit
                        </div>
                        <div class="btn btn-transparent red btn-outline btn-circle btn-sm disabled" data-value="remove" id="opt_remove" disabled="">
                            Remove
                        </div>
                        <div class="btn btn-transparent red btn-outline btn-circle btn-sm disabled" data-value="delete" id="opt_delete" disabled="">
                            Delete
                        </div>
                        <div class="btn btn-transparent yellow btn-outline btn-circle btn-sm" data-value="refresh" id="opt_refresh">
                            Refresh
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="portlet blue box">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>Menu </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"> </a>
                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="javascript:;" class="reload"> </a>
                            <a href="javascript:;" class="remove"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <ul class="nav nav-tabs" id="navmenu">
                            <?php
                            if (isset($modules) && !empty($modules)) {
                                foreach ($modules AS $k => $v) {
                                    $active = '';
                                    if ($v['id'] == 1) {
                                        $active = 'class="active"';
                                    }
                                    ?>
                                    <li <?php echo $active; ?>>
                                        <a data-module_id="<?php echo $v['id']; ?>" data-module_name="<?php echo $v['name']; ?>" href="#tab_2_<?php echo $v['id']; ?>" data-toggle="tab"> <?php echo $v['name']; ?> </a>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                        <div class="tab-content">
                            <?php
                            if (isset($modules) && !empty($modules)) {
                                foreach ($modules AS $k => $v) {
                                    $active2 = '';
                                    if ($v['id'] == 1) {
                                        $active2 = 'active';
                                    }
                                    ?>
                                    <div class="tab-pane fade <?php echo $active2; ?> in" id="tab_2_<?php echo $v['id']; ?>">
                                        <div id="tree_<?php echo $v['id']; ?>" class="tree-frm treeview"> </div>
                                        <!--<div class="alert alert-success no-margin margin-top-10"> Note! Opened and selected nodes will be saved in the user's browser, so when returning to the same tree the previous state will be restored. </div>-->
                                    </div>
                                    <?php
                                }
                            }
                            ?>					
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
<!-- /.modal -->
<div id="modal_add_edit" class="modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST" id="add_edit">
                <div class="modal-header">
                    <button type="button" class="close" data-action="close-modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="title_mdl"></h4>
                </div>
                <div class="modal-body">
                    <div class="scroller" style="height:300px" data-always-visible="1" data-rail-visible1="1">
                        <div class="row">
                            <div class="col-md-5">
								<div class="form-group">
                                    <label class="control-label">Module Name</label>
                                    <div class="input-icon right">
                                        <i class="fa fa-info-circle tooltips" data-container="body"></i>
                                        <input class="form-control" type="text" name="module_name" /> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Parent Name</label>
                                    <div class="input-icon right">
                                        <i class="fa fa-info-circle tooltips" data-container="body"></i>
                                        <input class="form-control" type="text" name="parent_name" /> 
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="control-label">Node Name</label>
                                    <div class="input-icon right">
                                        <i class="fa fa-info-circle tooltips" data-container="body"></i>
                                        <input class="form-control" type="text" name="name" /> 
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="control-label">Node Path</label>
                                    <div class="input-icon right">
                                        <i class="fa fa-info-circle tooltips"data-container="body"></i>
                                        <input class="form-control" type="text" name="path" /> 
                                    </div>
                                </div>
							</div>
							<div class="col-md-7">	
								<div class="form-group">
									<label>Icon</label>
									<select class="form-control" name="icon" id="icon">
										<option>-- select one --</option>
										<?php if(isset($icons) && !empty($icons)):?>
											<?php foreach($icons AS $key => $val):?>
												<option value="<?php echo $val['id'];?>"><i class="fa <?php echo $val['name'];?>"></i><?php echo $val['name'];?></option>
											<?php endforeach;?>
										<?php endif;?>
									</select>
								</div>							
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" rows="3" name="description"></textarea>
                                </div>
                                <div class="form-group" style="height:30px">
                                    <label>Active</label><br/>
                                    <input type="checkbox" class="make-switch" data-size="small" name="status"/>
                                </div><br/>
								 <div class="form-group" style="height:30px">
                                    <label>Is Logged In</label><br/>
                                    <input type="checkbox" class="make-switch" data-size="small" name="logged"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="text" name="id" hidden />
                    <input type="text" name="parent_id" hidden />
                    <input type="text" name="module_id" hidden />
                    <input type="text" name="level" hidden />
                    <button type="button" data-action="close-modal" class="btn dark btn-outline">Close</button>
                    <button type="submit" class="btn green" id="submit_add_edit">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>