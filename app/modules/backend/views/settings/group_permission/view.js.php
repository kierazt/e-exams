<script>
    var el = '#method';

    var fnAjaxBtn_select_all = function () {
        $(el).multiSelect('select_all');
    };

    var fnAjaxBtn_deselect_all = function () {
        $(el).multiSelect('deselect_all');
    };

    var fnAjaxBtn_refresh = function () {
        $(el).multiSelect('refresh');
    };

    var fnMultiSelect = function () {
        $(el).multiSelect({
            selectableHeader: "<div class='btn-group btn-group-xs btn-group-solid'><button type='button' class='btn green' onclick='fnAjaxBtn_select_all()' id='select-all'>select all</button></div>",
            selectionHeader: "<div class='btn-group btn-group-xs btn-group-solid'><button type='button' class='btn green' onclick='fnAjaxBtn_deselect_all()' id='deselect-all'>deselect all</button></div>",
        });
        return false;
    };

    var fnGetModule = function () {
        $.ajax({
            url: base_backend_url + 'settings/group_permission/get_module/',
            method: "POST", //First change type to method here
            success: function (response) {
                $('.module').html(response);
            },
            error: function (response) {
                $('.module').html(response);
            }
        });
        return false;
    };

    var fnGetGroup = function () {
        $.ajax({
            url: base_backend_url + 'settings/group_permission/get_group/',
            method: "POST", //First change type to method here
            success: function (response) {
                $('.group').html(response);
            },
            error: function (response) {
                $('.group').html(response);
            }
        });
        return false;
    };

    var fnGetMethod = function () {
        $.ajax({
            url: base_backend_url + 'settings/group_permission/get_method/',
            method: "POST", //First change type to method here
            success: function (response) {
                $('#method').html(response);
                fnMultiSelect();
            },
            error: function (response) {
                $('#method').html(response);
            }

        });
    }

    var TableDatatablesAjax = function () {
        return {
            //main function to initiate the module
            init: function () {
                fnToStr('view js ready!!!', 'success', 1100);
                $('.method_exist').on('click', function () {
                    var value = $(this).val();
                    console.log(value);
                    if (value == 1) {
                        $('#frmMltSlctMethod').show();
                        $('#frmMethodSelected').hide();
                    } else {
                        $('#frmMethodSelected').show();
                        $('#frmMltSlctMethod').hide();
                    }
                });
                $('.btn').on('click', function (e) {
                    e.preventDefault();
                    var value = $(this).attr('data-value');
                    if (value == "add" || value == "edit") {
                        fnGetModule();
                        fnGetGroup();
                        fnGetMethod();
                    }
                });
                var table = $('#datatable_ajax').DataTable({
                    "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                    'processing': true,
                    'language': {
                        'loadingRecords': '',
                        'processing': fnLoadingImg('loading.gif')
                    },
                    "sPaginationType": "bootstrap",
                    "paging": true,
                    "pagingType": "full_numbers",
                    "ordering": false,
                    "serverSide": true,
                    "ajax": {
                        url: base_backend_url + 'settings/group_permission/get_list/',
                        type: 'POST'
                    },
                    "columns": [
                        {"data": "rowcheck"},
                        {"data": "num"},
                        {"data": "group_name"},
                        {"data": "class"},
                        {"data": "action"},
                        {"data": "allowed"},
                        {"data": "public"},
                        {"data": "active"}
                    ],
                    "drawCallback": function (settings) {
                        $('.make-switch').bootstrapSwitch();
                        $('.select_tr').iCheck({
                            checkboxClass: 'icheckbox_minimal-grey',
                            radioClass: 'iradio_minimal-grey'
                        });
                    }
                });

                $('#datatable_ajax').on('switchChange.bootstrapSwitch', 'input[name="status"]', function (event, state) {
                    console.log(state); // true | false
                    var id = $(this).attr('data-id');
                    var formdata = {
						id: Base64.encode(id),
                        active: state
                    };
                    $.ajax({
                        url: base_backend_url + 'settings/group_permission/update_status/',
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            toastr.success('Successfully ' + response);
                            return false;
                        },
                        error: function () {
                            toastr.error('Failed ' + response);
                            return false;
                        }

                    });
                });
                $("#opt_edit").on('click', function () {
                    var status_ = $(this).hasClass('disabled');
                    if (status_ == 0) {
                        var id = $("input[class='select_tr']:checked").attr("data-id");
                        $.post(base_backend_url + 'settings/group_permission/get_data/' + id, function (data) {
                            var row = JSON.parse(data);
                            var status_ = false;
                            if (row.is_active == 1) {
                                status_ = true;
                            }

                            var allowed_ = false;
                            if (row.is_allowed == 1) {
                                allowed_ = true;
                            }

                            var public_ = false;
                            if (row.is_public == 1) {
                                public_ = true;
                            }
                            $('#frmMethodSelected').show();
                            $('#method_choice').hide();

                            $('input[name="id"]').val(row.id);
                            $('input[name="permission_id"]').val(row.permission_id);
                            $("#module option[value='" + row.module_id + "']").attr("selected", "selected");
                            $("#group option[value='" + row.group_id + "']").attr("selected", "selected");
                            $('#class').val(row.class);
                            $('input[name="method2"]').val(row.action);
                            $("[name='allowed']").bootstrapSwitch('state', allowed_);
                            $("[name='ispublic']").bootstrapSwitch('state', public_);
                            $("[name='status']").bootstrapSwitch('state', status_);
                            $('textarea[name="description"]').val(row.description);
                        });
                    }
                });

                $("#opt_delete").on('click', function () {
                    var status_ = $(this).hasClass('disabled');
                    if (status_ == 0) {
                        bootbox.confirm("Are you sure to delete this id?", function (result) {
                            if (result == true) {
                                var id = $("input[class='select_tr']:checked").attr("data-id");
                                var formdata = {
                                    id: Base64.encode(id)
                                };
                                $.ajax({
                                    url: base_backend_url + 'settings/group_permission/delete/',
                                    method: "POST", //First change type to method here
                                    data: formdata,
                                    success: function (response) {
                                        toastr.success('Successfully ' + response);
                                        fnCloseBootbox();
                                        fnCloseModal();
                                    },
                                    error: function () {
                                        toastr.error('Failed ' + response);
                                        fnCloseBootbox();
                                        fnCloseModal();
                                    }

                                });
                            } else {
                                toastr.success('Cancelling delete data ');
                                fnCloseBootbox();
                                fnCloseModal();
                                return false;
                            }
                        });

                    } else {
                        toastr.success('Something went wrong ');
                        fnCloseBootbox();
                        fnCloseModal();
                        return false;
                    }
                });

                $("#opt_remove").on('click', function () {
                    var status_ = $(this).hasClass('disabled');
                    if (status_ == 0) {
                        bootbox.confirm("Are you sure to remove this id?", function (result) {
                            if (result == true) {
                                var id = $("input[class='select_tr']:checked").attr("data-id");
                                var formdata = {
                                    id: Base64.encode(id)
                                };
                                $.ajax({
                                    url: base_backend_url + 'settings/group_permission/remove/',
                                    method: "POST", //First change type to method here
                                    data: formdata,
                                    success: function (response) {
                                        toastr.success('Successfully ' + response);
                                        fnCloseBootbox();
                                        fnCloseModal();
                                    },
                                    error: function () {
                                        toastr.error('Failed ' + response);
                                        fnCloseBootbox();
                                        fnCloseModal();
                                    }

                                });
                            } else {
                                toastr.success('Cancelling remove data ');
                                fnCloseBootbox();
                                fnCloseModal();
                                return false;
                            }
                        });
                    } else {
                        toastr.success('Something went wrong ');
                        fnCloseBootbox();
                        fnCloseModal();
                        return false;
                    }
                });

                $("#submit").on('click', function () {
                    var uri = base_backend_url + 'settings/group_permission/insert/';
                    var id = $('input[name="id"]').val();
                    var is_active = $('[name="status"]').bootstrapSwitch('state');
                    var is_allowed = $('[name="allowed"]').bootstrapSwitch('state');
                    var is_public = $('[name="ispublic"]').bootstrapSwitch('state');
                    var txt = 'add new group_permission';
					var method_exist = $("input[name='method_exist']:checked").val();
					var method = $('#method2').val();
					if(method_exist == 1){
						method = [];
						$.each($(".multi-select option:selected"), function () {
							method.push($(this).val());
						});
					}                    
                    var formdata = {
                        module_: $('#module').val(),
                        group_: $('#group').val(),
                        class_: $('input[name="class"]').val(),
                        method_: method,
                        is_active: is_active,
                        is_allowed: is_allowed,
                        is_public: is_public,
                        description: $('textarea[name="description"]').val()
                    };
                    if (id) {
                        uri = base_backend_url + 'settings/group_permission/update/';
                        txt = 'update group permission';
                        //method = $('input[name="method2"]').val();
                        formdata = {
                            id: Base64.encode(id),
                            permission_id: Base64.encode($('input[name="permission_id"]').val()),
                            module_: $('#module').val(),
                            group_: $('#group').val(),
                            class_: $('input[name="class"]').val(),
                            method_: method,
                            is_active: is_active,
                            is_allowed: is_allowed,
                            is_public: is_public,
                            description: $('textarea[name="description"]').val()
                        };
                    }
					//console.log(formdata);
					//console.log(uri);
					//return false;
                    $.ajax({
                        url: uri,
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            toastr.success('Successfully ' + txt);
                            fnCloseModal();
                        },
                        error: function () {
                            toastr.error('Failed ' + txt);
                            fnCloseModal();
                        }

                    });
                    return false;
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        TableDatatablesAjax.init();
    });
</script>