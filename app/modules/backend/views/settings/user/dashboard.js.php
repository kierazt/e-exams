<script>
    var Ajax = function () {
        return {
            //main function to initiate the module
            init: function () {
                toastr.success('Dashboard js ready!!!');
                var table = $('#ticket_dttable').DataTable({
                    "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                    "sPaginationType": "bootstrap",
                    "paging": true,
                    "pagingType": "full_numbers",
                    "ordering": false,
                    "serverSide": true,
                    "ajax": {
                        url: base_backend_url + 'ticket/master/get_list/',
                        type: 'POST'
                    },
                    "columns": [
                        {"data": "num"},
                        {"data": "code"},
                        {"data": "content"},
                        {"data": "status"},
                        {"data": "action"}
                    ],
                    "drawCallback": function (master) {
                        $('.make-switch').bootstrapSwitch();
                        $('.select_tr').iCheck({
                            checkboxClass: 'icheckbox_minimal-grey',
                            radioClass: 'iradio_minimal-grey'
                        });
                    }
                });
				$('table#ticket_dttable').on('click', 'td a.btn', function () {
                    var id = $(this).attr('data-id');
                    var href = $(this).attr('href');
                    $.post(base_backend_url + 'settings/user/get_ticket_detail/' + id, function (data) {
                        var row = JSON.parse(data);
                        var status_ = false;
                        if (row.is_active == 1) {
                            status_ = true;
                        }
                        if (href == '#response') {
                            //assign into response view
                            $('input[name="ticket_id"]').val(row.id);
                            $('input[name="ticket_code"]').val(row.code);
                            $('#code').html(row.code);
                            $('#create_date').html(row.create_date);
                            $('#content').html(row.content);
                        } else if (href == '#detail') {
                            //assign into detail view
                            $('input[name="create_date"]').val(row.create_date);
                            $('input[name="code"]').val(row.code);
                            $('input[name="ticket_status"]').val(row.ticket_status);
                            $('input[name="vendor_code"]').val(row.vendor_code);
                            $('input[name="vendor_name"]').val(row.vendor_name);
                            $('textarea[name="content"]').val(row.content);
                            $('textarea[name="description"]').val(row.description);
                        }
                    });
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        Ajax.init();
    });

</script>
