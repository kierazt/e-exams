<script>
    var TableDatatablesAjax = function () {
        return {
            //main function to initiate the module
            init: function () {
				fnToStr('ajax view js is ready','success');
                var table = $('#datatable_ajax').DataTable({
                    "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                    "sPaginationType": "bootstrap",
                    "paging": true,
                    "pagingType": "full_numbers",
                    "ordering": false,
                    "serverSide": true,
                    "ajax": {
                        url: base_backend_url + 'ticket/category/get_list/',
                        type: 'POST'
                    },
                    "columns": [
                        {"data": "rowcheck"},
                        {"data": "num"},
                        {"data": "name"},
                        {"data": "active"},
                        {"data": "description"}
                    ],
                    "drawCallback": function (master) {
                        $('.make-switch').bootstrapSwitch();
                    }
                });
				
				$('a.btn').on('click', function(){
					var action = $(this).attr('data-id');
					switch(action){
						case 'add': 
							$('.modal-title').html('Insert New Ticket Category');
						break;
						
						case 'edit':
							$('.modal-title').html('Update Exist Ticket Category');
							var status_ = $(this).hasClass('disabled');
							if (status_ == 0) {
								var formdata = {
									id: Base64.encode($("input[class='select_tr']:checked").attr("data-id"))
								};
								$.ajax({
									url: base_backend_url + 'ticket/category/get_data/',
									method: "POST", //First change type to method here
									data: formdata,
									success: function (response) {
										var row = JSON.parse(response);
										var status_ = false;
										if (row.is_active == 1) {
											status_ = true;
										}
										$('input[name="id"]').val(row.id);
										$('input[name="name"]').val(row.name);
										$("[name='status']").bootstrapSwitch('state', status_);
										$('textarea[name="description"]').val(row.description);
										$('#modal_add_edit').modal('show'); 
									},
									error: function () {
										fnToStr('Error is occured, please contact administrator.','error');
									}
								});
								return false;
							}
						break;
						
						case 'remove': 
						break;
						
						case 'delete': 
						break;
						
						case 'refresh': 
						break;
					}
				});
				
                
            }
        };

    }();

    jQuery(document).ready(function () {
        TableDatatablesAjax.init();
    });
</script>