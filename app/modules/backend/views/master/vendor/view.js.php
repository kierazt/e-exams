<script>
    var TableDatatablesAjax = function () {
        return {
            //main function to initiate the module
            init: function () {
                var table = $('#datatable_ajax').DataTable({
                    "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                    "sPaginationType": "bootstrap",
                    "paging": true,
                    "pagingType": "full_numbers",
                    "ordering": false,
                    "serverSide": true,
                    "ajax": {
                        url: base_backend_url + 'master/vendor/get_list/',
                        type: 'POST'
                    },
                    "columns": [
                        {"data": "rowcheck"},
                        {"data": "num"},
                        {"data": "name"},
                        {"data": "phone_number"},
                        {"data": "fax"},
                        {"data": "email"},
                        {"data": "active"},
                        {"data": "description"}
                    ],
                    "drawCallback": function (master) {
                        $('.make-switch').bootstrapSwitch();
                        $('.select_tr').iCheck({
                            checkboxClass: 'icheckbox_minimal-grey',
                            radioClass: 'iradio_minimal-grey'
                        });
                    }
                });

                $('#datatable_ajax').on('switchChange.bootstrapSwitch', 'input[name="status"]', function (event, state) {
                    console.log(state); // true | false
                    var id = $(this).attr('data-id');
                    var formdata = {
						id:Base64.encode(id),
                        active: state
                    };
                    $.ajax({
                        url: base_backend_url + 'master/vendor/update_status/',
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            toastr.success('Successfully ' + response);
                            return false;
                        },
                        error: function () {
                            toastr.error('Failed ' + response);
                            return false;
                        }

                    });
                });

                $("#opt_edit").on('click', function () {
                    var status_ = $(this).hasClass('disabled');
                    if (status_ == 0) {
                        var id = $("input[class='select_tr']:checked").attr("data-id");
                        $.post(base_backend_url + 'master/vendor/get_data/' + id, function (data) {
                            var row = JSON.parse(data);
                            var status_ = false;
                            if (row.is_active == 1) {
                                status_ = true;
                            }
							var logged_ = false;
                            if (row.is_logged_in == 1) {
                                logged_ = true;
                            }
                            $('input[name="id"]').val(row.id);
                            $('input[name="user_name"]').val(row.username);
                            $('input[name="first_name"]').val(row.first_name);
                            $('input[name="last_name"]').val(row.last_name);
                            $('input[name="email"]').val(row.email);
                            $("[name='status']").bootstrapSwitch('state', status_);
                            $("[name='logged']").bootstrapSwitch('state', logged_);
							$('#modal_add_edit').fadeIn();
                        });
                    }
                });

                $("#opt_delete").on('click', function () {
                    var status_ = $(this).hasClass('disabled');
                    if (status_ == 0) {
                        bootbox.confirm("Are you sure to delete this id?", function (result) {
                            if (result == true) {
                                var id = $("input[class='select_tr']:checked").attr("data-id");
                                var formdata = {
                                    id: Base64.encode(id)
                                };
                                $.ajax({
                                    url: base_backend_url + 'master/vendor/delete/',
                                    method: "POST", //First change type to method here
                                    data: formdata,
                                    success: function (response) {
                                        toastr.success('Successfully ' + response);
                                        fnCloseBootbox();
                                        fnCloseModal();
                                    },
                                    error: function () {
                                        toastr.error('Failed ' + response);
                                        fnCloseBootbox();
                                        fnCloseModal();
                                    }

                                });
                            } else {
                                toastr.success('Cancelling delete data ');
                                fnCloseBootbox();
                                fnCloseModal();
                                return false;
                            }
                        });

                    } else {
                        toastr.success('Something went wrong ');
                        fnCloseBootbox();
                        fnCloseModal();
                        return false;
                    }
                });

                $("#opt_remove").on('click', function () {
                    var status_ = $(this).hasClass('disabled');
                    if (status_ == 0) {
                        bootbox.confirm("Are you sure to remove this id?", function (result) {
                            if (result == true) {
                                var id = $("input[class='select_tr']:checked").attr("data-id");
                                var formdata = {
                                    id: Base64.encode(id)
                                };
                                $.ajax({
                                    url: base_backend_url + 'master/vendor/remove/',
                                    method: "POST", //First change type to method here
                                    data: formdata,
                                    success: function (response) {
                                        toastr.success('Successfully ' + response);
                                        fnCloseBootbox();
                                        fnCloseModal();
                                    },
                                    error: function () {
                                        toastr.error('Failed ' + response);
                                        fnCloseBootbox();
                                        fnCloseModal();
                                    }

                                });
                            } else {
                                toastr.success('Cancelling remove data ');
                                fnCloseBootbox();
                                fnCloseModal();
                                return false;
                            }
                        });
                    } else {
                        toastr.success('Something went wrong ');
                        fnCloseBootbox();
                        fnCloseModal();
                        return false;
                    }
                });
                $("#add_edit").submit(function () {
                    var id = $('input[name="id"]').val();
                    var is_active = $("[name='status']").bootstrapSwitch('state');
                    var is_logged = $("[name='logged']").bootstrapSwitch('state');
                    var uri = base_backend_url + 'master/vendor/insert/';
                    var txt = 'add new vendor';
                    var formdata = {
                        username: $('input[name="username"]').val(),
                        first_name: $('input[name="first_name"]').val(),
                        last_name: $('input[name="last_name"]').val(),
                        email: $('input[name="email"]').val(),
                        active: is_active,
						logged: is_logged
                    };
                    if (id)
                    {
                        uri = base_backend_url + 'master/vendor/update/';
                        txt = 'update vendor';
                        formdata = {
                            id: Base64.encode(id),
                            name: $('input[name="name"]').val(),
                            description: $('textarea[name="description"]').val(),
                            active: is_active
                        };
                    }
                    $.ajax({
                        url: uri,
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            toastr.success('Successfully ' + txt);
                            fnCloseModal();
                        },
                        error: function () {
                            toastr.error('Failed ' + txt);
                            fnCloseModal();
                        }

                    });
                    return false;
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        TableDatatablesAjax.init();
    });
</script>