<?php

class User extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function login() {
        $data = $this->setup_layout();
        $data['title_for_layout'] = 'welcome';
        //load ajax var
        $var = array(
            array(
                'keyword' => 'wew',
                'value' => '1234'
            )
        );
        $this->load_ajax_var($var);

        //load js
        $js_files = array(
            static_url('templates/metronics/assets/global/plugins/jquery-validation/js/jquery.validate.min.js'),
            static_url('templates/metronics/assets/global/plugins/jquery-validation/js/additional-methods.min.js'),
            static_url('templates/metronics/assets/global/plugins/select2/js/select2.full.min.js'),
            static_url('templates/metronics/assets/global/plugins/backstretch/jquery.backstretch.min.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic_login.phtml', $data);
    }

    public function auth() {
        $this->load->library('oreno_auth');
        $post = $this->input->post(NULL, TRUE);
        if (isset($post['login']) && !empty($post['login'])) {
            $auth = $this->oreno_auth->auth($post['login']);
            $result = json_decode($auth);
            echo return_call_back('message', array('login' => $result->result->status), 'json');
        } else {
            echo 'failed';
        }
        exit();
    }

    public function dashboard() {
        $data['logs'] = $this->get_all_history();
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'),
        );
        $this->load_js($js_files);
        $data['title_for_layout'] = 'welcome to dashbohard';
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function logout() {
        $this->oreno_auth->destroy_session($this->_session_auth());
        $this->session->set_flashdata('success', 'Successfully logout from system!');
        redirect(base_backend_url('login'));
    }
public function get_ticket_detail($id = null) {
        $this->load->model('Tbl_helpdesk_tickets');
        $res = $this->Tbl_helpdesk_tickets->find('first', array(
			'fields' => array('a.*', 'c.name ticket_status', 'f.id vendor_id', 'f.code vendor_code', 'f.name vendor_name'),
            'conditions' => array('a.id' => $id),
			'order' => array('key' => 'a.create_date', 'type' => 'ASC'),
			'joins'=>array(
                array(
                    'table' => 'tbl_helpdesk_ticket_transactions b',
                    'conditions' => 'b.ticket_id = a.id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_helpdesk_ticket_status c',
                    'conditions' => 'c.id = b.status_id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_helpdesk_vendor_users d',
                    'conditions' => 'd.category_id = b.category_id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_helpdesk_ticket_categories e',
                    'conditions' => 'e.id = b.category_id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_helpdesk_vendors f',
                    'conditions' => 'f.id = d.vendor_id',
                    'type' => 'left'
                )
            )
        ));
        if (isset($res) && !empty($res)) {
            if ($res['create_date']) {
                $res['create_date'] = idn_date(strtotime($res['create_date']));
            }
            echo json_encode($res);
        } else {
            echo null;
        }
    }
}
