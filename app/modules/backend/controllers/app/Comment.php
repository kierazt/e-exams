<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Comment extends MY_Controller {

//put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model(array('Tbl_cms_comments'));
    }

    public function index() {
        redirect(base_developer_url('app/comment/view/'));
    }

    public function view() {
        $data['title_for_layout'] = 'welcome';
        $data['content'] = 'ini kontent web';
        $data['view-header-title'] = 'View Group List';
        if (isset($search) && !empty($search)) {
			$cond['or_like'] = array('a.module' => $search, 'a.class' => $search, 'a.action' => $search, 'a.is_active' => $search);
			$cond_count['or_like'] = $cond['or_like'];
		}
		$js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'),
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function get_list() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $draw = $post['draw'];
            $start = $post['start'];
            $length = $post['length'];
            $search = trim($post['search']['value']);

            $this->load->library('pagination');
            $get = $this->input->get();
            $cond_count = array();
            if (isset($search) && !empty($search)) {
                $cond['like'] = array('name', $search);
                $cond['or_like'] = array('is_active', $search);
                $cond_count = $cond['or_like'];
            }
            $cond['fields'] = array('a.*', 'b.title content_title', 'b.is_active content_status', 'b.is_page');
            $cond['limit'] = array('perpage' => $length, 'offset' => $start);
            $cond['joins'] = array(
                array(
                    'table' => 'tbl_cms_contents b',
                    'conditions' => 'a.content_id = b.id',
                    'type' => 'left'
                )
            );
            $total_rows = $this->Tbl_cms_comments->find('count', $cond_count);
            $config = array(
                'base_url' => base_developer_url('app/comment/view/'),
                'total_rows' => $total_rows,
                'per_page' => $length,
            );
            $this->pagination->initialize($config);
            $res = $this->Tbl_cms_comments->find('all', $cond);
            $arr = array();
            if (isset($res) && !empty($res)) {
                $i = $start + 1;
                foreach ($res as $d) {
                    $status = '';
                    if ($d['is_active'] == 1) {
                        $status = 'checked';
                    }
                    $content_is_page = '';
                    if ($d['content_status'] == 1) {
                        $content_is_page = 'checked';
                    }
                    $action_status = '<div class="form-comment">
									<div class="col-md-9" style="height:30px">
										<input type="checkbox" class="make-switch" data-size="small" data-value="' . $d['is_active'] . '" data-id="' . $d['id'] . '" name="status" ' . $status . '/>
									</div>
								</div>';
                    $action_content_is_page = '<div class="form-comment">
									<div class="col-md-9" style="height:30px">
										<input type="checkbox" class="make-switch" data-size="small" data-value="' . $d['content_status'] . '" data-id="' . $d['id'] . '" name="content_status" ' . $content_is_page . '/>
									</div>
								</div>';
                    $data['rowcheck'] = '<input type="checkbox" class="select_tr" name="select_tr[' . $d['id'] . ']" data-id="' . $d['id'] . '" />';
                    $data['num'] = $i;
                    $data['text'] = $d['text']; //optional	
                    $data['content_title'] = $d['content_title']; //optional	
                    $data['content_is_page'] = $action_content_is_page; //optional	
                    $data['reason_for_block'] = $d['reason_for_block']; //optional					
                    $data['active'] = $action_status; //optional					
                    $data['description'] = $d['description']; //optional
                    $arr[] = $data;
                    $i++;
                }
            }
            $output = array(
                'draw' => $draw,
                'recordsTotal' => $total_rows,
                'recordsFiltered' => $total_rows,
                'data' => $arr,
            );
            //output to json format
            echo json_encode($output);
        } else {
            echo json_encode(array());
        }
    }

    public function get_data($id = null) {
        $res = $this->Tbl_cms_comments->find('first', array(
            'conditions' => array('id' => $id)
        ));
        if (isset($res) && !empty($res)) {
            echo json_encode($res);
        } else {
            echo null;
        }
    }

    public function insert() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $status = 0;
            if ($post['active'] == 'true') {
                $status = 1;
            }
            $arr_insert = array(
                'name' => $post['name'],
                'description' => $post['description'],
                'is_active' => $status,
                'created_by' => (int) base64_decode($this->auth_config->user_id),
                'create_date' => date_now()
            );
            $res = $this->Tbl_cms_comments->insert($arr_insert);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        } else {
            echo 'failed';
        }
    }

    public function update() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $status = 0;
            if ($post['active'] == "true") {
                $status = 1;
            }
            $arr = array(
                'name' => $post['name'],
                'description' => $post['description'],
                'is_active' => $status
            );
            $res = $this->Tbl_cms_comments->update($arr, base64_decode($post['id']));
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        } else {
            echo 'failed';
        }
    }

    public function update_status($id_ = null) {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $id = base64_decode($id_);
            $status = 0;
            if ($post['active'] == "true") {
                $status = 1;
            }
            $arr = array(
                'is_active' => $status
            );
            $res = $this->Tbl_cms_comments->update($arr, $id);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

    public function remove() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $id = base64_decode($post['id']);
            $res = $this->Tbl_cms_comments->remove($id);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

    public function delete() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $id = base64_decode($post['id']);
            $res = $this->Tbl_cms_comments->delete($id);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

}
