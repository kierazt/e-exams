<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Content extends MY_Controller {

//put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model(array('Tbl_cms_contents'));
    }

    public function index() {
        redirect(base_url('backend/app/content/view/'));
    }

    public function view() {
        $data['title_for_layout'] = 'welcome';
        $data['content'] = 'ini kontent web';
        $data['view-header-title'] = 'View Group List';
        $css_files = array(
            static_url('lib/packages/summernote/dist/summernote-lite.css')
        );
        $this->load_css($css_files);
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'),
            static_url('lib/packages/summernote/dist/summernote-lite.js'),
            static_url('lib/packages/summernote/dist/plugin/summernote-ext-elfinder.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function get_list() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $draw = $post['draw'];
            $start = $post['start'];
            $length = $post['length'];
            $search = trim($post['search']['value']);

            $this->load->library('pagination');
            $get = $this->input->get();
            $cond_count = array();
            $cond['table'] = 'Tbl_cms_contents a';
            if (isset($search) && !empty($search)) {
                $cond['like'] = array('name', $search);
                $cond['or_like'] = array('is_active', $search);
                $cond_count = $cond['or_like'];
            }
            $cond['fields'] = array('a.*', 'b.name menu_name');
            $cond['limit'] = array('perpage' => $length, 'offset' => $start);
            $cond['joins'] = array(
                array(
                    'table' => 'tbl_menus b',
                    'conditions' => 'b.id = a.menu_id',
                    'type' => 'left'
                )
            );
            $total_rows = $this->Tbl_cms_contents->find('count', $cond_count);
            $config = array(
                'base_url' => base_url('backend/app/content/view/'),
                'total_rows' => $total_rows,
                'per_page' => $length,
            );
            $this->pagination->initialize($config);
            $res = $this->Tbl_cms_contents->find('all', $cond);
            $arr = array();
            if (isset($res) && !empty($res)) {
                $i = $start + 1;
                foreach ($res as $d) {
                    $status = '';
                    if ($d['is_page'] == 1) {
                        $status = 'checked';
                    }
                    $content = '';
                    if ($d['is_page'] == 1) {
                        $content = 'checked';
                    }
                    $action_content = '<div class="form-group">
									<div class="col-md-9" style="height:30px">
										<input type="checkbox" class="make-switch" data-size="small" data-value="' . $d['is_page'] . '" data-id="' . $d['id'] . '" name="is_page" ' . $content . '/>
									</div>
								</div>';
                    $action_status = '<div class="form-group">
						<div class="col-md-9" style="height:30px">
							<input type="checkbox" class="make-switch" data-size="small" data-value="' . $d['is_active'] . '" data-id="' . $d['id'] . '" name="status" ' . $status . '/>
						</div>
					</div>';
                    $data['rowcheck'] = '<input type="checkbox" class="select_tr" name="select_tr[' . $d['id'] . ']" data-id="' . $d['id'] . '" />';
                    //'<div class="checkbox"><input type="checkbox" class="minimal-red checkbox_act" name="select_tr[' . $d['id'] . ']" data-id="' . $d['id'] . '" id="select_tr' . $d['id'] . '" value="' . $d['id'] . '"/></div>';

                    $data['num'] = $i;
                    $data['title'] = $d['title']; //optional	
                    $data['text'] = $d['text']; //optional	
                    $data['meta_keyword'] = $d['meta_keyword']; //optional	
                    $data['meta_description'] = $d['meta_description']; //optional	
                    $data['menu'] = $d['menu_name']; //optional	
                    $data['content_status'] = $action_content; //optional	
                    $data['active'] = $action_status; //optional	
                    $data['action'] = '<a href="' . base_url('app/content/edit_content/' . $d['id']) . '">edit Page content</a>'; //optional					
                    $data['description'] = $d['description']; //optional
                    $arr[] = $data;
                    $i++;
                }
            }
            $output = array(
                'draw' => $draw,
                'recordsTotal' => $total_rows,
                'recordsFiltered' => $total_rows,
                'data' => $arr,
            );
            //output to json format
            echo json_encode($output);
        } else {
            echo json_encode(array());
        }
    }

    public function get_data($id = null) {
        $res = $this->Tbl_cms_contents->find('first', array(
            'conditions' => array('id' => $id)
        ));
        if (isset($res) && !empty($res)) {
            echo json_encode($res);
        } else {
            echo null;
        }
    }

    public function insert() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $status = 0;
            if ($post['active'] == 'true') {
                $status = 1;
            }
            $arr_insert = array(
                'title' => $post['title'],
                'text' => $_POST['content'],
                'menu_id' => $post['menu'],
                'description' => $post['description'],
                'is_active' => $status,
                'created_by' => (int) base64_decode($this->auth_config->user_id),
                'create_date' => date_now()
            );
            $id = $this->Tbl_cms_contents->insert_return_id($arr_insert);
            if ($id) {
                // $filename = base64_encode($id);
                // $data_ = str_replace('"', "'", $_POST['content']);
                // $this->load->library(array('oreno_save_to_file'));
                // $save_content = $this->oreno_save_to_file->save($filename, ($data_));
                // $content = '';
                // if($save_content == true){
                // $content = $filename;
                // }
                // $update_data = array(
                // 'content' => $filename.'.log',
                // );
                // $this->Tbl_cms_contents->update($update_data, $id);
                echo 'success';
            } else {
                echo 'failed';
            }
        } else {
            echo 'failed';
        }
    }

    public function update() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $status = 0;
            if ($post['active'] == "true") {
                $status = 1;
            }
            $arr = array(
                'title' => $post['title'],
                'menu_id' => $post['menu'],
                'description' => $post['description'],
                'is_active' => $status,
            );
            $res = $this->Tbl_cms_contents->update($arr, base64_decode($post['id']));
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        } else {
            echo 'failed';
        }
    }

    public function update_status($id_ = null) {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $id = base64_decode($id_);
            $status = 0;
            if ($post['active'] == "true") {
                $status = 1;
            }
            $arr = array(
                'is_active' => $status
            );
            $res = $this->Tbl_cms_contents->update($arr, $id);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

    public function remove($id_ = null) {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $id = base64_decode($id_);
            $res = $this->Tbl_cms_contents->remove($id);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

    public function get_menu() {
        $this->load->model('Tbl_menus');
        $res = $this->Tbl_menus->find('all', array('conditions' => array('level' => 1, 'module_master_id' => 4, 'is_active' => 1)));
        if (isset($res) && !empty($res)) {
            $arr = '';
            foreach ($res AS $k => $v) {
                $arr .= '<option value="' . $v['id'] . '">' . $v['name'] . '</option>';
            }
            echo $arr;
        } else {
            echo null;
        }
    }

    public function add_content() {
        $this->load->model('Tbl_cms_contents');
        $this->load->helper('fn_view_helper');
        $data['title_for_layout'] = 'welcome';
        $data['view-header-title'] = 'View Group List';

        $css_files = array(
            static_url('lib/packages/summernote/dist/summernote.css')
        );
        $this->load_css($css_files);
        $js_files = array(
            static_url('includes/script/summernote/dist/summernote.js'),
            static_url('includes/script/summernote/dist/plugin/summernote-ext-elfinder.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function edit_content($id = null) {
        $this->load->model('Tbl_cms_contents');
        $this->load->helper('fn_view_helper');
        $data['title_for_layout'] = 'welcome';
        $data['content'] = $this->Tbl_cms_contents->find('first', array(
            'conditions' => array('id' => $id)
        ));
        $data['view-header-title'] = 'View Group List';
        $data['id'] = $id;
        $css_files = array(
            static_url('lib/packages/summernote/dist/summernote.css')
        );
        $this->load_css($css_files);
        $js_files = array(
            static_url('includes/script/summernote/dist/summernote.js'),
            static_url('includes/script/summernote/dist/plugin/summernote-ext-elfinder.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function update_content() {
        if (isset($_POST) && !empty($_POST)) {
            $status = 0;
            $arr = array(
                'text' => $_POST['content']
            );
            $res = $this->Tbl_cms_contents->update($arr, base64_decode($_POST['id']));
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        } else {
            echo 'failed';
        }
    }

}
