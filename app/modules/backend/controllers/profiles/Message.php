<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Message
 *
 * @author root
 */
class Message extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function fn_get_all_message($post = array()) {
        $this->load->model('Tbl_component_messages');
        if (isset($post) && !empty($post)) {
            $draw = $post['draw'];
            $start = $post['start'];
            $length = $post['length'];
            $search = trim($post['search']['value']);
            $this->load->library('pagination');
            $cond_count = array();
            if (isset($search) && !empty($search)) {
                $cond['like'] = array('name', $search);
                $cond['or_like'] = array('is_active', $search);
                $cond_count = $cond['or_like'];
            }
            $cond['conditions'] = array('a.is_active' => 1, 'a.status_id !=' => 3);
            $cond['fields'] = array('a.*', 'b.name category_name', 'c.name status_name', 'd.name label_name');
            $cond['limit'] = array('perpage' => $length, 'offset' => $start);
            $cond['joins'] = array(
                array(
                    'table' => 'tbl_component_message_categories b',
                    'conditions' => 'b.id = a.category_id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_component_message_status c',
                    'conditions' => 'c.id = a.status_id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_component_message_labels d',
                    'conditions' => 'd.id = a.label_id',
                    'type' => 'left'
                )
            );
            $total_rows = $this->Tbl_component_messages->find('count', $cond_count);
            $config = array(
                'base_url' => base_backend_url('profiles/message/get_all_message/'),
                'total_rows' => $total_rows,
                'per_page' => $length,
            );
            $this->pagination->initialize($config);
            $res = $this->Tbl_component_messages->find('all', $cond);
            $arr = array();
            if (isset($res) && !empty($res)) {
                $i = $start + 1;
                foreach ($res as $d) {
                    $status = '';
                    if ($d['is_active'] == 1) {
                        $status = 'checked';
                    }
                    $action_status = '<div class="form-category">
                        <div class="col-md-9" style="height:30px">
                            <input type="checkbox" class="make-switch" data-size="small" data-value="' . $d['is_active'] . '" data-id="' . $d['id'] . '" name="status" ' . $status . '/>
                        </div>
                    </div>';
                    $data['rowcheck'] = '<input type="checkbox" class="select_tr" name="select_tr[' . $d['id'] . ']" data-id="' . $d['id'] . '" />';
                    $data['num'] = $i;
                    $data['sender'] = $this->get_user($d['id'], 'email'); //optional		
                    $data['message'] = substr($d['message'], 100); //optional					
                    $data['attachment'] = '<a href="">download</a>'; //optional
                    $arr[] = $data;
                    $i++;
                }
            }
            $output = array(
                'status' => 1,
                'draw' => $draw,
                'recordsTotal' => $total_rows,
                'recordsFiltered' => $total_rows,
                'data' => $arr,
            );
            //output to json format
            echo json_encode($output);
        } else {
            echo json_encode(array('status' => 0));
        }
    }

    public function get_user($id = null, $keyword = null){        
        $this->load->model('Tbl_users');
        $res = $this->Tbl_users->find('first', array(
            'conditions' => array('id' => $id)
        ));
        if($res){
            return $res[$keyword];
        }else{
            return null;
        }
    }
}
