<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="<?php echo isset($_ajax_var_ticket->open) ? $_ajax_var_ticket->open : 0 ; ?>"><?php echo isset($_ajax_var_ticket->open) ? $_ajax_var_ticket->open : 0 ; ?></span>
                </div>
                <div class="desc"> Open Tickets </div>
            </div>
            <a class="more" href="javascript:;"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat red">
            <div class="visual">
                <i class="fa fa-bar-chart-o"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="<?php echo isset($_ajax_var_ticket->progress) ? $_ajax_var_ticket->progress : 0 ; ?>"><?php echo isset($_ajax_var_ticket->progress) ? $_ajax_var_ticket->progress : 0 ; ?></span></div>
                <div class="desc"> Progress Tickets </div>
            </div>
            <a class="more" href="javascript:;"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat green">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="<?php echo isset($_ajax_var_ticket->transfer) ? $_ajax_var_ticket->transfer : 0 ; ?>"><?php echo isset($_ajax_var_ticket->transfer) ? $_ajax_var_ticket->transfer : 0 ; ?></span>
                </div>
                <div class="desc"> Transfer Tickets </div>
            </div>
            <a class="more" href="javascript:;"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat purple">
            <div class="visual">
                <i class="fa fa-globe"></i>
            </div>
            <div class="details">
                <div class="number"> +
                    <span data-counter="counterup" data-value="<?php echo isset($_ajax_var_ticket->close) ? $_ajax_var_ticket->close : 0 ; ?>"><?php echo isset($_ajax_var_ticket->close) ? $_ajax_var_ticket->close : 0 ; ?></span></div>
                <div class="desc"> Close Tickets </div>
            </div>
            <a class="more" href="javascript:;"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>

    <div class="col-md-12 col-sm-12">
        <div class="portlet light tasks-widget bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-green-haze hide"></i>
                    <span class="caption-subject font-green bold uppercase">Tickets</span>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover table-checkable" id="ticket_dttable">
                    <thead>
                        <tr role="row" class="heading">
                            <th width="2%"><input type="checkbox" data-checkbox="icheckbox_minimal-grey" class="group-checkable" name="select_all"/></th>
							<th width="5%"> # </th>
							<th width="15%"> No Ticket </th>
							<th width="15%"> Issue </th>
							<th width="15%"> Create Date </th>
							<th width="15%"> Status </th>
							<th width="15%"> Action </th>
                        </tr>							
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-blue"></i>
                    <span class="caption-subject font-blue bold uppercase">Recent Activities</span>
                </div>
                <div class="actions">
                    <div class="btn-group">
                        <a class="btn btn-sm blue btn-outline btn-circle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> Filter By
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
                            <label>
                                <div class="checker"><span><input type="checkbox"></span></div> Finance</label>
                            <label>
                                <div class="checker"><span class="checked"><input type="checkbox" checked=""></span></div> Membership</label>
                            <label>
                                <div class="checker"><span><input type="checkbox"></span></div> Customer Support</label>
                            <label>
                                <div class="checker"><span class="checked"><input type="checkbox" checked=""></span></div> HR</label>
                            <label>
                                <div class="checker"><span><input type="checkbox"></span></div> System</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 300px;">
                    <div class="scroller" style="height: 300px; overflow: hidden; width: auto;" data-always-visible="1" data-rail-visible="0" data-initialized="1">
                        <ul class="feeds"><?php echo isset($logs) ? $logs : ''; ?></ul>
                    </div>
                    <div class="slimScrollBar" style="background: rgb(187, 187, 187) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; height: 155.979px;"></div>
                    <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(234, 234, 234) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                </div>
                <div class="scroller-footer">
                    <div class="btn-arrow-link pull-right">
                        <a href="javascript:;">See All Records</a>
                        <i class="icon-arrow-right"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('includes/tools/detail_ticket.html.php'); ?>
<?php $this->load->view('includes/tools/response_ticket.html.php'); ?>