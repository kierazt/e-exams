<script>
    var Ajax = function () {
        return {
            //main function to initiate the module
            init: function () {
                toastr.success('Dashboard js ready!!!');
                $('table#ticket_dttable').on('click', 'td a.btn', function () {
                    var id = $(this).attr('data-id');
                    var href = $(this).attr('href');
                    $.post(base_url + 'vendor/user/get_ticket_detail/' + id, function (data) {
                        var row = JSON.parse(data);
                        var status_ = false;
                        if (row.is_active == 1) {
                            status_ = true;
                        }
                        if (href == '#response') {
                            //assign into response view
                            $('input[name="ticket_id"]').val(row.id);
                            $('input[name="ticket_code"]').val(row.code);
                            $('#code').html(row.code);
                            $('#create_date').html(row.create_date);
                            $('#content').html(row.content);
                        } else if (href == '#detail') {
                            //assign into detail view
                            $('input[name="create_date"]').val(row.create_date);
                            $('input[name="code"]').val(row.code);
                            $('input[name="ticket_status"]').val(row.ticket_status);
                            $('input[name="vendor_code"]').val(row.vendor_code);
                            $('input[name="vendor_name"]').val(row.vendor_name);
                            $('textarea[name="content"]').val(row.content);
                            $('textarea[name="description"]').val(row.description);
                        }
                    });
                });
                $('input[type="checkbox"][name="insert_by"]').on('click', function () {
                    var id = $(this).prop("checked");
                    if (id == true) {
                        $.post(base_url + 'vendor/user/get_issue_suggest/' + id, function (data) {
                            $('#issue_template').html(data);
                            $('#insert_from_temp').fadeIn();
                        });
                    } else {
                        $('#insert_from_temp').fadeOut();
                    }
                });
                $('#issue_template').on('change', function () {
                    var value = $(this).val();
                    $('textarea[name="message"]').val(value);
					$('#issue_template').prop('selectedIndex',0);
                });
                $('input[type="checkbox"][name="agree"]').on('click', function () {
                    var id = $(this).prop("checked");
                    if (id == true) {
                        $('#sbmt_form').fadeIn();
                    } else {
                        $('#sbmt_form').fadeOut();
                    }
                });
                var table = $('#ticket_dttable').DataTable({
                    "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                    "sPaginationType": "bootstrap",
                    "paging": true,
                    "pagingType": "full_numbers",
                    "ordering": false,
                    "serverSide": true,
                    "ajax": {
                        url: base_url + 'vendor/ticket/get_list/open',
                        type: 'POST'
                    },
                    "columns": [
                         {"data": "rowcheck"},
						{"data": "num"},
						{"data": "code"},
						{"data": "content"},
						{"data": "create"},
						{"data": "status"},
						{"data": "action"}	
                    ],
                    "drawCallback": function (master) {
                        $('.make-switch').bootstrapSwitch();
                        $('.select_tr').iCheck({
                            checkboxClass: 'icheckbox_minimal-grey',
                            radioClass: 'iradio_minimal-grey'
                        });
                    }
                });

                $('#sbmt_form').on('click', function () {
                    App.startPageLoading();
                    var uri = base_url + 'vendor/ticket/tracking';
                    var ticket_code = $('input[name="ticket_code"]').val();
                    var formdata = {
                        ticket_id: Base64.encode($('input[name="ticket_id"]').val()),
                        ticket_code: ticket_code,
                        message: $('textarea[name="message"]').val()
                    };
                    $.ajax({
                        url: uri,
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            App.stopPageLoading();
                            toastr.success('Successfully add new ticket data ');
                            window.location.href = base_url + 'vendor/tracking/view/' + Base64.encode(ticket_code);
                            return false;
                        },
                        error: function () {
                            App.stopPageLoading();
                            toastr.success('Failed add new ticket data ');
                            return false;
                        }

                    });
                    return false;
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        Ajax.init();
    });

</script>
