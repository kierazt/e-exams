<?php

class User extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        redirect(base_url('login'));
    }

    public function login($template_id = '') {
        $data = $this->setup_layout();
        $data['title_for_layout'] = 'welcome';
        //load ajax var
        $var = array(
            array(
                'keyword' => 'wew',
                'value' => '1234'
            )
        );
        $this->load_ajax_var($var);

        //load js
        $js_files = array(
            static_url('templates/metronics/assets/global/plugins/jquery-validation/js/jquery.validate.min.js'),
            static_url('templates/metronics/assets/global/plugins/jquery-validation/js/additional-methods.min.js'),
            static_url('templates/metronics/assets/global/plugins/select2/js/select2.full.min.js'),
            static_url('templates/metronics/assets/global/plugins/backstretch/jquery.backstretch.min.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic_login.phtml', $data);
    }

    public function auth() {
        $this->load->library('oreno_auth');
        $post = $this->input->post(NULL, TRUE);
        if (isset($post['login']) && !empty($post['login'])) {
            $auth = $this->oreno_auth->auth($post['login']);
            $result = json_decode($auth);
            echo return_call_back('message', array('login' => $result->result->status), 'json');
        } else {
            echo 'failed';
        }
        exit();
    }

    public function dashboard() {
        $this->load->model('Tbl_helpdesk_ticket_issue_suggestions');
        $data['logs'] = $this->get_all_history();
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'),
        );
        $this->load_js($js_files);
        $data['title_for_layout'] = 'welcome to dashbohard';
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function logout() {
        $this->oreno_auth->destroy_session($this->_session_auth());
        $this->session->set_flashdata('success', 'Successfully logout from system!');
        redirect(base_url('lecturer/login'));
    }

    public function my_profile() {
        $data['title_for_layout'] = 'welcome';
        //load ajax var
        $var = array(
            array(
                'keyword' => 'wew',
                'value' => '1234'
            )
        );
        $this->load_ajax_var($var);
        //load js
        $js_files = array(
            static_url('templates/metronics/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')
        );

        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function my_inbox() {
        $data['title_for_layout'] = 'welcome';
        //load ajax var
        $var = array(
            array(
                'keyword' => 'wew',
                'value' => '1234'
            )
        );
        $this->load_ajax_var($var);
        //load class
        $css_files = array(
            static_url('templates/metronics/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css'),
            static_url('templates/metronics/assets/global/plugins/fancybox/source/jquery.fancybox.css'),
            static_url('templates/metronics/assets/global/css/plugins.min.css'),
            static_url('templates/metronics/assets/apps/css/inbox.min.css')
        );
        $this->load_css($css_files);
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function my_task() {
        $data['title_for_layout'] = 'welcome';
        //load ajax var
        $var = array(
            array(
                'keyword' => 'wew',
                'value' => '1234'
            )
        );
        $this->load_ajax_var($var);
        //load js
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function my_notif() {
        $data['title_for_layout'] = 'welcome';
        //load ajax var
        $var = array(
            array(
                'keyword' => 'wew',
                'value' => '1234'
            )
        );
        $this->load_ajax_var($var);
        //load js
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function lock_screen() {
        $this->oreno_auth->lock_screen();
        $data['title_for_layout'] = 'welcome';
        //load ajax var
        $var = array(
            array(
                'keyword' => 'wew',
                'value' => '1234'
            )
        );
        $this->load_ajax_var($var);
        //load js
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic_lock_screen.phtml', $data);
    }

    public function un_lock_screen() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $res = $this->oreno_auth->unlock_screen($post);
            if ($res) {
                echo return_call_back('message', array('verify' => true), 'json');
            } else {
                echo return_call_back('message', array('verify' => false), 'json');
            }
        } else {
            echo return_call_back('message', array('verify' => false), 'json');
        }
        exit();
    }

    public function get_all_message() {
        echo 'wew';
    }

    public function get_ticket_detail($id = null) {
        $this->load->model('Tbl_helpdesk_tickets');
        $res = $this->Tbl_helpdesk_tickets->find('first', array(
			'fields' => array('a.*', 'c.name ticket_status', 'f.id vendor_id', 'f.code vendor_code', 'f.name vendor_name'),
            'conditions' => array('a.id' => $id),
			'order' => array('key' => 'a.create_date', 'type' => 'ASC'),
			'joins'=>array(
                array(
                    'table' => 'tbl_helpdesk_ticket_transactions b',
                    'conditions' => 'b.ticket_id = a.id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_helpdesk_ticket_status c',
                    'conditions' => 'c.id = b.status_id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_helpdesk_vendor_users d',
                    'conditions' => 'd.category_id = b.category_id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_helpdesk_ticket_categories e',
                    'conditions' => 'e.id = b.category_id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_helpdesk_vendors f',
                    'conditions' => 'f.id = d.vendor_id',
                    'type' => 'left'
                )
            )
        ));
        if (isset($res) && !empty($res)) {
            if ($res['create_date']) {
                $res['create_date'] = idn_date(strtotime($res['create_date']));
            }
            echo json_encode($res);
        } else {
            echo null;
        }
    }

    public function get_issue_suggest() {
        $this->load->model('Tbl_helpdesk_ticket_issue_suggestions');
        $res = $this->Tbl_helpdesk_ticket_issue_suggestions->find('all', array(
            'conditions' => array('a.is_active' => 1)
        ));
        if (isset($res) && !empty($res)) {
            $ar = '<option>-- select one --</option>';
            foreach ($res AS $key => $val) {
                $txt = '';
                if($_SESSION['_lang']){
                    if($_SESSION['_lang'] == 'english'){
                        $txt = $val['value_eng'];
                    }elseif($_SESSION['_lang'] == 'indonesian'){
                        $txt = $val['value_ina'];
                    }
                }
                $ar .= '<option value="' . $txt . '">' . $txt . '</option>';
            }
            echo ($ar);
        } else {
            echo null;
        }
    }

}
