<?php

class User extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->get_all_history();
    }

    public function index() {
        redirect(base_url('login'));
    }

    public function login($template_id = '') {
        $data = $this->setup_layout();
        $data['title_for_layout'] = 'welcome';
        //load ajax var
        $var = array(
            array(
                'keyword' => 'wew',
                'value' => '1234'
            )
        );
        $this->load_ajax_var($var);

        //load js
        $js_files = array(
            static_url('templates/metronics/assets/global/plugins/jquery-validation/js/jquery.validate.min.js'),
            static_url('templates/metronics/assets/global/plugins/jquery-validation/js/additional-methods.min.js'),
            static_url('templates/metronics/assets/global/plugins/select2/js/select2.full.min.js'),
            static_url('templates/metronics/assets/global/plugins/backstretch/jquery.backstretch.min.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic_login.phtml', $data);
    }

    public function auth() {
        $this->load->library('oreno_auth');
        $post = $this->input->post(NULL, TRUE);
        if (isset($post['login']) && !empty($post['login'])) {
            $auth = $this->oreno_auth->auth($post['login']);
            $result = json_decode($auth);
            echo return_call_back('message', array('login' => $result->result->status), 'json');
        } else {
            echo 'failed';
        }
        exit();
    }

    public function dashboard() {
        $data['logs'] = $this->get_all_history();
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'),
        );
        $this->load_js($js_files);
        $data['title_for_layout'] = 'welcome to dashbohard';
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function logout() {
        $this->_logout();
        redirect(base_url('login'));
    }

    public function my_profile() {
        $data['title_for_layout'] = 'welcome';
        //load ajax var
        $var = array(
            array(
                'keyword' => 'wew',
                'value' => '1234'
            )
        );
        $this->load_ajax_var($var);
        //load js
        $js_files = array(
            static_url('templates/metronics/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')
        );

        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function my_inbox() {
        $data['title_for_layout'] = 'welcome';
        //load ajax var
        $var = array(
            array(
                'keyword' => 'wew',
                'value' => '1234'
            )
        );
        $this->load_ajax_var($var);
        //load class
        $css_files = array(
            static_url('templates/metronics/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css'),
            static_url('templates/metronics/assets/global/plugins/fancybox/source/jquery.fancybox.css'),
            static_url('templates/metronics/assets/global/css/plugins.min.css'),
            static_url('templates/metronics/assets/apps/css/inbox.min.css')
        );
        $this->load_css($css_files);
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function my_task() {
        $data['title_for_layout'] = 'welcome';
        //load ajax var
        $var = array(
            array(
                'keyword' => 'wew',
                'value' => '1234'
            )
        );
        $this->load_ajax_var($var);
        //load js
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function my_notif() {
        $data['title_for_layout'] = 'welcome';
        //load ajax var
        $var = array(
            array(
                'keyword' => 'wew',
                'value' => '1234'
            )
        );
        $this->load_ajax_var($var);
        //load js
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function lock_screen() {
        $this->oreno_auth->lock_screen();
        $data['title_for_layout'] = 'welcome';
        //load ajax var
        $var = array(
            array(
                'keyword' => 'wew',
                'value' => '1234'
            )
        );
        $this->load_ajax_var($var);
        //load js
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic_lock_screen.phtml', $data);
    }

    public function un_lock_screen() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $res = $this->oreno_auth->unlock_screen($post);
            if ($res) {
                echo return_call_back('message', array('verify' => true), 'json');
            } else {
                echo return_call_back('message', array('verify' => false), 'json');
            }
        } else {
            echo return_call_back('message', array('verify' => false), 'json');
        }
        exit();
    }

    public function switch_lang() {
        $request_language = $this->input->get(NULL, TRUE);
        if (isset($request_language['bahasa']) && !empty($request_language['bahasa'])) {
            $session_language = $this->session->userdata('_lang');
            if ($request_language['bahasa'] != $session_language) {
                unset($_SESSION['_lang']);
                $_SESSION['_lang'] = $request_language['bahasa'];
                redirect($this->agent->referrer());
            }
        } else {
            $this->session->set_flashdata('error', $this->lang->line('failed_change_lang'));
            redirect($this->agent->referrer());
        }
    }

    

}
