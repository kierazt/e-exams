<?php
$res = $this->session->userdata('_lang');
if (isset($res) && !empty($res)) {
	if ($res == "english") {
		$bhs = "Language";
		$lng = "English";
		$val = '<option value="' . $res . '">' . $lng . '</option><option value="indonesian">Indonesia</option>';
	} elseif ($res == "indonesian") {
		$bhs = "Bahasa";
		$lng = "Indonesia";
		$val = '<option value="' . $res . '">' . $lng . '</option><option value="english">English</option>';
	}
} else {
	$val = '<option value="english">English</option><option value="indonesian">Bahasa Indonesia</option>';
}
?>
<form action="<?php echo base_url('helpdesk/user/switch_lang'); ?>" name="lang_select" method="get"> 
	<select class="form-control" name="bahasa" onchange="document.forms['lang_select'].submit();" name="lang">
		<?php echo $val; ?>
	</select>
</form>