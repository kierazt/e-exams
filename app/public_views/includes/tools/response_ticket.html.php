<div class="modal fade bs-modal-lg" id="response" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="form-horizontal" role="form" id="submit_response">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <center><h4 class="modal-title">Response and Take an open ticket</h4></center>
                </div>
                <div class="modal-body">
                    <div style="padding:20px">
                        <div class="form-group form-md-line-input">
                            <label for="form_control_1">Ticket Number</label>
                            <h4 id="code"></h4>
                        </div>
                        <div>
                            <blockquote>
                                <p id="content"></p>
                            </blockquote>
                        </div>
						<hr/>
                        <div class="form-group form-md-line-input">
                            <label for="form_control_1">Ticket Create Date</label>
                            <h4 id="create_date"></h4>
                        </div>
						<hr/>
                        <div class="form-group form-md-line-input">
                            <div class="md-checkbox-inline">
                                <div class="md-checkbox ">
                                    <input type="checkbox" id="insert_by2" name="insert_by" value="2" class="md-check">
                                    <label for="insert_by2">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span> Insert message from issue suggestions  
                                    </label>
                                </div>
                            </div>
                        </div>
						<hr/>
                        <div class="form-group form-md-line-input has-info" hidden id="insert_from_temp">
                            <select class="form-control" id="issue_template"></select>
                            <label for="form_control_1">Issue Suggestion message</label>
                        </div>
                        <div class="form-group form-md-line-input form-md-floating-label">
                            <textarea class="form-control" rows="3" id="message" name="message" placeholder="Input your response message"></textarea>
                        </div>
						<hr/>
                        <div class="form-group form-md-checkboxes">
                            <label>Aggreement</label>
                            <div class="md-checkbox-inline">
                                <div class="md-checkbox has-success">
                                    <input type="checkbox" name="agree" id="agree" class="md-check">
                                    <label for="agree">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span> aggre to take this open ticket, and make a quick response
									</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="text" name="ticket_id" hidden />
                    <input type="text" name="ticket_code" hidden />
                    <button type="button" class="btn green" id="sbmt_form" style="display:none">Submit</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>