-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 24 Apr 2019 pada 20.02
-- Versi Server: 5.6.11
-- Versi PHP: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `db_online_exams`
--
CREATE DATABASE IF NOT EXISTS `db_online_exams` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `db_online_exams`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_ajax_funcs`
--

CREATE TABLE IF NOT EXISTS `tbl_ajax_funcs` (
  `id` int(32) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_ajax_funcs`
--

INSERT INTO `tbl_ajax_funcs` (`id`, `keyword`, `value`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'global_uri', '''var global_uri = function(link){\r\n                return "'' . global_uri($this->config->module_name) . ''"+link\r\n            };''', '-', 1, 1, '2019-01-25 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_cms_categories`
--

CREATE TABLE IF NOT EXISTS `tbl_cms_categories` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `parent_id` int(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `created_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_cms_category_contents`
--

CREATE TABLE IF NOT EXISTS `tbl_cms_category_contents` (
  `id` int(32) NOT NULL,
  `content_id` int(32) NOT NULL,
  `content_category_id` int(32) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(32) NOT NULL,
  `created_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_cms_comments`
--

CREATE TABLE IF NOT EXISTS `tbl_cms_comments` (
  `id` int(32) NOT NULL,
  `text` text NOT NULL,
  `content_id` int(32) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `reason_for_block` text NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_cms_contents`
--

CREATE TABLE IF NOT EXISTS `tbl_cms_contents` (
  `id` int(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  `meta_keyword` text NOT NULL,
  `meta_description` text NOT NULL,
  `is_footer` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) DEFAULT '0',
  `is_page` tinyint(1) NOT NULL DEFAULT '0',
  `menu_id` int(32) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_cms_contents`
--

INSERT INTO `tbl_cms_contents` (`id`, `title`, `text`, `meta_keyword`, `meta_description`, `is_footer`, `is_active`, `is_page`, `menu_id`, `description`, `created_by`, `create_date`) VALUES
(1, 'Home', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '', 0, 1, 1, 1, '-', 1, '2018-11-14 10:35:25'),
(2, 'About', ' <aside class="f_widget ab_widget">\r\n                    <div class="f_title">\r\n                        <h3>About Me</h3>\r\n                    </div>\r\n                    <p>If you own an Iphone, you’ve probably already worked out how much fun it is to use it to watch movies-it has that nice big screen, and the sound quality.</p>\r\n                </aside>', '', '', 1, 1, 1, 2, '-', 1, '2018-12-18 11:38:30'),
(3, 'Contact', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '', 0, 1, 1, 3, '-', 1, '2018-11-14 10:35:25'),
(4, 'Galleries', 'The standard Lorem Ipsum passage, used since the 1500s\r\n\r\n"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."', '', '', 0, 1, 1, 4, '-', 1, '2018-12-18 11:38:30'),
(5, 'Events', '<aside class="f_widget news_widget">\r\n    <div class="feature_inner row event_list">\r\n\r\n</div>  <br/>\r\n\r\n<center><div id="pagination_ajax"></div></center>\r\n</aside>', '', '', 1, 1, 1, 5, '-', 1, '2018-12-18 11:38:30'),
(6, 'Socials', '<aside class="f_widget social_widget">\r\n                    <div class="f_title">\r\n                        <h3>Follow Me</h3>\r\n                    </div>\r\n                    <p>Let us be social</p>\r\n                    <ul class="list">\r\n                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>\r\n                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>\r\n                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>\r\n                        <li><a href="#"><i class="fa fa-behance"></i></a></li>\r\n                    </ul>\r\n                </aside>', '', '', 1, 1, 1, 0, '-', 1, '2018-12-18 11:38:30'),
(7, 'Dashboard', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '', 0, 1, 1, 11, '-', 1, '2018-11-14 10:35:25'),
(8, 'Profile', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '', 1, 1, 1, 12, '-', 1, '2018-12-18 11:38:30'),
(9, 'Event', '<aside class="f_widget news_widget">\r\n    <div class="table-container">\r\n        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">\r\n            <thead>\r\n                <tr role="row" class="heading">\r\n                    <th width="5%"> # </th>\r\n                    <th width="15%"> Event Name </th>\r\n                    <th width="15%"> Registration Date Start  </th>\r\n                    <th width="15%"> Registration Date End </th>\r\n                    <th width="15%"> Event Date </th>\r\n                    <th width="15%"> Total Participate </th>\r\n                    <th width="200"> Description </th>\r\n                    <th width="200"> Action </th>\r\n                </tr>							\r\n            </thead>\r\n            <tbody></tbody>\r\n        </table>\r\n    </div>   \r\n</aside>', '', '', 0, 1, 1, 13, '-', 1, '2018-11-14 10:35:25'),
(10, 'History', 'The standard Lorem Ipsum passage, used since the 1500s\r\n\r\n"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."', '', '', 0, 1, 1, 14, '-', 1, '2018-12-18 11:38:30'),
(11, 'News', '<aside class="f_widget news_widget">\r\n                    <div class="f_title">\r\n                        <h3>Newsletter</h3>\r\n                    </div>\r\n                    <p>Stay updated with our latest trends</p>\r\n                    <div id="mc_embed_signup">\r\n                        <form target="_blank" method="post" class="subscribes">\r\n                            <div class="input-group d-flex flex-row">\r\n                                <input name="EMAIL" placeholder="Enter email address" onfocus="this.placeholder = ''''" onblur="this.placeholder = ''Email Address ''" required="" type="email">\r\n                                <button class="btn sub-btn"><span class="lnr lnr-arrow-right"></span></button>		\r\n                            </div>				\r\n                            <div class="mt-10 info"></div>\r\n                        </form>\r\n                    </div>\r\n                </aside>', '', '', 1, 1, 1, 15, '-', 1, '2018-12-18 11:38:30'),
(12, 'Socials', '<aside class="f_widget social_widget">\r\n                    <div class="f_title">\r\n                        <h3>Follow Me</h3>\r\n                    </div>\r\n                    <p>Let us be social</p>\r\n                    <ul class="list">\r\n                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>\r\n                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>\r\n                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>\r\n                        <li><a href="#"><i class="fa fa-behance"></i></a></li>\r\n                    </ul>\r\n                </aside>', '', '', 1, 1, 1, 16, '-', 1, '2018-12-18 11:38:30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_cms_content_photos`
--

CREATE TABLE IF NOT EXISTS `tbl_cms_content_photos` (
  `id` int(32) NOT NULL,
  `path` varchar(255) NOT NULL,
  `content_id` int(32) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(32) NOT NULL,
  `created_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_component_messages`
--

CREATE TABLE IF NOT EXISTS `tbl_component_messages` (
  `id` int(32) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `from_id` int(32) NOT NULL,
  `to_id` int(32) NOT NULL,
  `status_id` int(32) NOT NULL,
  `category_id` int(32) DEFAULT NULL,
  `label_id` int(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_component_messages`
--

INSERT INTO `tbl_component_messages` (`id`, `subject`, `content`, `from_id`, `to_id`, `status_id`, `category_id`, `label_id`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'test', 'wew lah pokona', 1, 2, 1, NULL, 1, 1, 1, '2019-02-12 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_component_message_categories`
--

CREATE TABLE IF NOT EXISTS `tbl_component_message_categories` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_component_message_labels`
--

CREATE TABLE IF NOT EXISTS `tbl_component_message_labels` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_component_message_labels`
--

INSERT INTO `tbl_component_message_labels` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'general', '-', 1, 1, '2019-02-12 00:00:00'),
(2, 'bussiness', '-', 1, 1, '2019-02-12 00:00:00'),
(3, 'news', '-', 1, 1, '2019-02-12 00:00:00'),
(4, 'important', '-', 1, 1, '2019-02-12 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_component_message_status`
--

CREATE TABLE IF NOT EXISTS `tbl_component_message_status` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_component_message_status`
--

INSERT INTO `tbl_component_message_status` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'unread', '-', 1, 1, '2019-02-11 00:00:00'),
(2, 'read', '-', 1, 1, '2019-02-11 00:00:00'),
(3, 'archive', '-', 1, 1, '2019-02-11 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_component_notifications`
--

CREATE TABLE IF NOT EXISTS `tbl_component_notifications` (
  `id` int(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `description` text NOT NULL,
  `label` varchar(255) NOT NULL,
  `category_id` int(32) DEFAULT NULL,
  `status_id` int(32) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_component_notifications`
--

INSERT INTO `tbl_component_notifications` (`id`, `title`, `content`, `description`, `label`, `category_id`, `status_id`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'server not responding', 'server return 500 in browser', '-', 'label-success', 2, 1, 1, 1, '2019-02-02 00:00:00'),
(2, 'Application error', 'website error when access using chrome and show php error', '-', 'label-danger', 3, 1, 1, 1, '2019-02-02 00:00:00'),
(3, 'Database overload', 'web show db overload notice and cannot access', '-', 'label-warning', 4, 1, 1, 1, '2019-02-02 00:00:00'),
(4, 'IP 12.123.2.122 user blok with server', 'IP 12.123.2.122 user blok with server', '-', 'label-info', 1, 1, 1, 1, '2019-02-02 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_component_notification_categories`
--

CREATE TABLE IF NOT EXISTS `tbl_component_notification_categories` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_component_notification_categories`
--

INSERT INTO `tbl_component_notification_categories` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'network', '-', 1, 1, '2019-02-02 00:00:00'),
(2, 'server', '-', 1, 1, '2019-02-02 00:00:00'),
(3, 'system', '-', 1, 1, '2019-02-02 00:00:00'),
(4, 'database', '-', 1, 1, '2019-02-02 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_component_notification_status`
--

CREATE TABLE IF NOT EXISTS `tbl_component_notification_status` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_component_notification_status`
--

INSERT INTO `tbl_component_notification_status` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'pending', '-', 1, 1, '2019-02-02 00:00:00'),
(2, 'read', '-', 1, 1, '2019-02-02 00:00:00'),
(3, 'replied', '-', 1, 1, '2019-02-02 00:00:00'),
(4, 'archive', '-', 1, 1, '2019-02-02 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_component_tasks`
--

CREATE TABLE IF NOT EXISTS `tbl_component_tasks` (
  `id` int(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `progress` int(3) NOT NULL,
  `description` text NOT NULL,
  `status_id` int(32) NOT NULL,
  `category_id` int(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_component_task_categories`
--

CREATE TABLE IF NOT EXISTS `tbl_component_task_categories` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_component_task_status`
--

CREATE TABLE IF NOT EXISTS `tbl_component_task_status` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_configs`
--

CREATE TABLE IF NOT EXISTS `tbl_configs` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `is_static` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data untuk tabel `tbl_configs`
--

INSERT INTO `tbl_configs` (`id`, `keyword`, `value`, `is_static`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'app_name', 'online exams', 0, 1, 1, '2018-07-23 00:00:00'),
(2, 'lang_id', '28173tgfds7tf', 0, 1, 1, '2018-07-23 00:00:00'),
(3, 'lang_name', 'english', 0, 1, 1, '2018-07-23 00:00:00'),
(4, 'salt', '834red567gh4765vbfr76538', 0, 1, 1, '2018-07-23 00:00:00'),
(5, 'session_name', 'd98786tayghdjaw90d87atw', 0, 1, 1, '2018-07-23 00:00:00'),
(6, 'website_id', 'd9s8a7yudhioas987dyuhss', 0, 1, 1, '2018-07-23 00:00:00'),
(7, 'cookie_id', '90daw786tyghdjioaw987d6', 0, 1, 1, '2018-07-23 00:00:00'),
(8, 'redirect_success_login_backend', 'backend/dashboard', 0, 1, 1, '2018-07-23 00:00:00'),
(9, 'redirect_failed_login_backend', 'backend/login', 0, 1, 1, '2018-07-23 00:00:00'),
(10, 'mod_active', 'frontend', 0, 1, 1, '2018-11-04 00:00:00'),
(11, 'controller_active', 'home', 0, 1, 1, '2018-11-04 00:00:00'),
(12, 'global_title_en', 'welcome to online exams', 0, 1, 1, '2018-11-09 07:53:48'),
(13, 'layout_frontend', 'metronic', 0, 1, 1, '2018-11-16 20:32:48'),
(14, 'uri_img_item_color', 'media/img/items/colors/', 0, 1, 1, '2019-01-03 09:45:11'),
(15, 'uri_img_item_brand', 'media/img/items/brands/', 0, 1, 1, '2019-01-03 09:45:41'),
(16, 'dev_status', '1', 0, 1, 1, '2019-02-01 00:00:00'),
(17, 'footer_about', ' <aside class="f_widget ab_widget">\r\n                    <div class="f_title">\r\n                        <h3>About Me</h3>\r\n                    </div>\r\n                    <p>If you own an Iphone, you’ve probably already worked out how much fun it is to use it to watch movies-it has that nice big screen, and the sound quality.</p>\r\n                </aside>', 1, 1, 1, '2019-02-12 00:00:00'),
(18, 'footer_newsletter', '<aside class="f_widget news_widget">\r\n                    <div class="f_title">\r\n                        <h3>Newsletter</h3>\r\n                    </div>\r\n                    <p>Stay updated with our latest trends</p>\r\n                    <div id="mc_embed_signup">\r\n                        <form target="_blank" method="post" class="subscribes">\r\n                            <div class="input-group d-flex flex-row">\r\n                                <input name="EMAIL" placeholder="Enter email address" onfocus="this.placeholder = ''''" onblur="this.placeholder = ''Email Address ''" required="" type="email">\r\n                                <button class="btn sub-btn"><span class="lnr lnr-arrow-right"></span></button>		\r\n                            </div>				\r\n                            <div class="mt-10 info"></div>\r\n                        </form>\r\n                    </div>\r\n                </aside>', 1, 1, 1, '2019-02-12 00:00:00'),
(19, 'footer_socials', '<aside class="f_widget social_widget">\r\n                    <div class="f_title">\r\n                        <h3>Follow Me</h3>\r\n                    </div>\r\n                    <p>Let us be social</p>\r\n                    <ul class="list">\r\n                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>\r\n                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>\r\n                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>\r\n                        <li><a href="#"><i class="fa fa-behance"></i></a></li>\r\n                    </ul>\r\n                </aside>', 1, 1, 1, '2019-02-12 00:00:00'),
(20, 'session_helpdesk', 'd98789iu8ghdjaw90d80po9', 0, 1, 1, '2018-07-23 00:00:00'),
(21, 'login_lecturer_layout', '3', 0, 1, 1, '2019-03-27 00:00:00'),
(22, 'login_backend_layout', '1', 0, 1, 1, '2019-03-27 00:00:00'),
(23, 'login_frontend_layout', '4', 0, 1, 1, '2019-03-27 00:00:00'),
(24, 'api_key', '98ufu83476yrhdge', 0, 1, 1, '2019-03-27 00:00:00'),
(25, 'api_user', 'api_09283hdjks', 0, 1, 1, '2019-03-27 00:00:00'),
(26, 'api_pass', '098eq7312&_DSA', 0, 1, 1, '2019-03-27 00:00:00'),
(27, 'copyright', '2019 © Telkom Signet', 0, 1, 1, '2019-03-27 00:00:00'),
(28, 'login_developer_layout', '2', 0, 1, 1, '2019-03-27 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_correct_answer`
--

CREATE TABLE IF NOT EXISTS `tbl_correct_answer` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `id_type` int(32) NOT NULL,
  `id_question` int(32) NOT NULL,
  `value` text NOT NULL,
  `created_by` int(32) NOT NULL,
  `created_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_dosen`
--

CREATE TABLE IF NOT EXISTS `tbl_dosen` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(155) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `no_hp` int(32) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `created_by` int(32) NOT NULL,
  `created_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `tbl_dosen`
--

INSERT INTO `tbl_dosen` (`id`, `first_name`, `last_name`, `alamat`, `no_hp`, `jabatan`, `created_by`, `created_date`) VALUES
(1, '', '', '', 0, '', 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_email_configs`
--

CREATE TABLE IF NOT EXISTS `tbl_email_configs` (
  `id` int(32) NOT NULL,
  `protocol` varchar(255) NOT NULL,
  `host` varchar(255) NOT NULL,
  `port` varchar(255) NOT NULL,
  `user` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `mailtype` varchar(255) NOT NULL,
  `charset` varchar(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_email_configs`
--

INSERT INTO `tbl_email_configs` (`id`, `protocol`, `host`, `port`, `user`, `pass`, `mailtype`, `charset`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'smtp', 'smtp.gmail.com', '587', 'firman.begin@gmail.com', 'Ab1234abcd', 'html', 'iso-8859-1', 1, 1, '2019-02-27 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_email_layout`
--

CREATE TABLE IF NOT EXISTS `tbl_email_layout` (
  `id` int(32) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_email_layout`
--

INSERT INTO `tbl_email_layout` (`id`, `keyword`, `value`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'user_activation', ' <center>[date]</center><br/>                         Pengguna yang terhormat,                         <br/>                         <br/>                         Terima kasih telah melakukan registrasi akun di pesky indosporttiming, berikut detail data akun anda :                         email       : [email]<br/>                         username    : [username]<br/>                         password    : [password]<br/>                         status      : tidak aktif<br/>                         <br/>                             Untuk aktivasi account klik <b>[activation_link]</b><br/>                         Akun yang belum di aktifkan tidak akan bisa melakukan pendaftaran event lomba atau login kedalam dashboard indosporttiming<br/>                         Mohon untuk tidak men-sharing atau berbagi pakai dengan pihak lain terhadap akun dengan data diri anda agar tidak terjadi hal - hal yang tidak di inginkan.<br/>                     ', '-', 1, 1, '2019-02-27 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_email_links`
--

CREATE TABLE IF NOT EXISTS `tbl_email_links` (
  `id` int(32) NOT NULL,
  `email_layout_id` int(32) NOT NULL,
  `keyword` int(255) NOT NULL,
  `value` text NOT NULL,
  `is_active` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_exams`
--

CREATE TABLE IF NOT EXISTS `tbl_exams` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `code` varchar(8) NOT NULL,
  `question_id` int(32) NOT NULL,
  `dosen_id` int(32) NOT NULL,
  `matkul_id` int(32) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `max` int(5) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_exam_users`
--

CREATE TABLE IF NOT EXISTS `tbl_exam_users` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `user_id` int(32) NOT NULL,
  `exam_id` int(32) NOT NULL,
  `result_id` int(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_global_variables`
--

CREATE TABLE IF NOT EXISTS `tbl_global_variables` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_groups`
--

CREATE TABLE IF NOT EXISTS `tbl_groups` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `tbl_groups`
--

INSERT INTO `tbl_groups` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'superuser', '-', 1, 1, '2019-02-25 00:00:00'),
(2, 'student', '-', 1, 1, '2019-02-12 00:00:00'),
(3, 'lecturer', '-', 1, 1, '2019-02-12 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_group_permissions`
--

CREATE TABLE IF NOT EXISTS `tbl_group_permissions` (
  `id` int(32) NOT NULL,
  `group_id` int(32) NOT NULL,
  `permission_id` int(32) NOT NULL,
  `is_allowed` tinyint(1) NOT NULL DEFAULT '0',
  `is_public` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_group_permissions`
--

INSERT INTO `tbl_group_permissions` (`id`, `group_id`, `permission_id`, `is_allowed`, `is_public`, `is_active`, `created_by`, `create_date`) VALUES
(1, 1, 1, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(2, 1, 2, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(3, 1, 3, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(4, 1, 4, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(5, 1, 5, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(6, 1, 6, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(7, 1, 7, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(8, 1, 8, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(9, 1, 9, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(10, 1, 10, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(11, 1, 11, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(12, 1, 12, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(13, 1, 13, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(14, 1, 14, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(15, 1, 15, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(16, 1, 16, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(17, 1, 17, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(18, 1, 18, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(19, 1, 19, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(20, 1, 20, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(21, 1, 21, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(22, 1, 22, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(23, 1, 23, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(24, 1, 24, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(25, 1, 25, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(26, 1, 26, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(27, 1, 27, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(28, 1, 28, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(29, 1, 29, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(30, 1, 30, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(31, 1, 31, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(32, 1, 32, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(33, 1, 33, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(34, 1, 34, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(35, 1, 35, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(36, 1, 36, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(37, 1, 37, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(38, 1, 38, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(39, 1, 39, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(40, 1, 40, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(41, 1, 41, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(42, 1, 42, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(43, 1, 43, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(44, 1, 44, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(45, 1, 45, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(46, 1, 46, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(47, 1, 47, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(48, 1, 48, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(49, 1, 49, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(50, 1, 50, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(51, 1, 51, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(52, 1, 52, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(53, 1, 53, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(54, 1, 54, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(55, 1, 55, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(56, 1, 56, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(57, 1, 57, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(58, 1, 58, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(59, 1, 59, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(60, 1, 60, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(61, 1, 61, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(62, 1, 62, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(63, 1, 63, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(64, 1, 64, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(65, 1, 65, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(66, 1, 66, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(67, 1, 67, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(68, 1, 68, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(69, 1, 69, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(70, 1, 69, 1, 0, 1, 1, '2019-04-16 00:00:00'),
(71, 1, 70, 1, 0, 1, 1, '2019-04-16 15:55:12'),
(72, 1, 71, 1, 0, 1, 1, '2019-04-16 15:55:12'),
(73, 1, 72, 1, 0, 1, 1, '2019-04-16 15:55:12'),
(74, 1, 73, 1, 0, 1, 1, '2019-04-16 15:55:13'),
(75, 1, 74, 1, 0, 1, 1, '2019-04-16 15:55:13'),
(76, 1, 75, 1, 0, 1, 1, '2019-04-16 15:55:13'),
(77, 1, 76, 1, 0, 1, 1, '2019-04-16 15:55:13'),
(78, 1, 77, 1, 0, 1, 1, '2019-04-16 15:55:13'),
(79, 2, 78, 1, 0, 1, 1, '2019-04-17 19:36:48'),
(80, 2, 79, 1, 0, 1, 1, '2019-04-17 19:38:30'),
(81, 2, 80, 1, 0, 1, 1, '2019-04-17 19:39:03'),
(82, 2, 81, 1, 0, 1, 1, '2019-04-17 19:39:17'),
(83, 2, 82, 1, 0, 1, 1, '2019-04-17 19:39:28'),
(84, 2, 83, 1, 0, 1, 1, '2019-04-17 19:39:41'),
(85, 2, 84, 1, 0, 1, 1, '2019-04-17 19:39:56'),
(86, 2, 85, 1, 0, 1, 1, '2019-04-17 19:40:12'),
(87, 2, 86, 1, 0, 1, 1, '2019-04-17 19:40:58'),
(88, 2, 87, 1, 0, 1, 1, '2019-04-17 19:41:15'),
(89, 2, 88, 1, 0, 1, 1, '2019-04-17 19:41:30'),
(90, 2, 89, 1, 0, 1, 1, '2019-04-17 19:41:53'),
(91, 2, 90, 1, 0, 1, 1, '2019-04-17 19:42:24'),
(92, 2, 91, 1, 0, 1, 1, '2019-04-17 19:42:35'),
(93, 2, 92, 1, 0, 1, 1, '2019-04-17 19:53:27'),
(94, 2, 93, 1, 0, 1, 1, '2019-04-17 21:45:17'),
(95, 2, 94, 1, 0, 1, 1, '2019-04-18 15:57:32'),
(96, 1, 95, 1, 0, 1, 1, '2019-04-20 20:33:31'),
(97, 1, 96, 1, 0, 1, 1, '2019-04-20 20:33:31'),
(98, 1, 97, 1, 0, 1, 1, '2019-04-20 20:33:31'),
(99, 1, 98, 1, 0, 1, 1, '2019-04-20 20:33:32'),
(100, 1, 99, 1, 0, 1, 1, '2019-04-20 20:33:32'),
(101, 1, 100, 1, 0, 1, 1, '2019-04-20 20:33:32'),
(102, 1, 101, 1, 0, 1, 1, '2019-04-20 20:33:32'),
(103, 1, 102, 1, 0, 1, 1, '2019-04-20 20:33:32'),
(104, 1, 103, 1, 0, 1, 1, '2019-04-20 20:33:32'),
(105, 1, 104, 1, 0, 1, 1, '2019-04-20 20:33:32'),
(106, 1, 105, 1, 0, 1, 1, '2019-04-20 20:33:32'),
(107, 1, 106, 1, 0, 1, 1, '2019-04-20 20:33:32'),
(108, 1, 107, 1, 0, 1, 1, '2019-04-20 20:33:32'),
(109, 1, 108, 1, 0, 1, 1, '2019-04-21 09:02:57'),
(110, 1, 109, 1, 0, 1, 1, '2019-04-21 09:02:57'),
(111, 1, 110, 1, 0, 1, 1, '2019-04-21 09:02:57'),
(112, 1, 111, 1, 0, 1, 1, '2019-04-21 09:02:57'),
(113, 1, 112, 1, 0, 1, 1, '2019-04-21 09:02:57'),
(114, 1, 113, 1, 0, 1, 1, '2019-04-21 09:02:57'),
(115, 1, 114, 1, 0, 1, 1, '2019-04-21 09:02:57'),
(116, 1, 115, 1, 0, 1, 1, '2019-04-21 09:02:57'),
(117, 1, 116, 1, 0, 1, 1, '2019-04-21 09:02:57'),
(118, 1, 117, 1, 0, 1, 1, '2019-04-21 09:02:58'),
(119, 1, 118, 1, 0, 1, 1, '2019-04-21 09:02:58'),
(120, 1, 119, 1, 0, 1, 1, '2019-04-21 09:02:58'),
(121, 1, 120, 1, 0, 1, 1, '2019-04-21 09:02:58'),
(122, 1, 121, 1, 0, 1, 1, '2019-04-21 19:24:21'),
(123, 1, 122, 1, 0, 1, 1, '2019-04-21 19:24:21'),
(124, 1, 123, 1, 0, 1, 1, '2019-04-21 19:24:21'),
(125, 1, 124, 1, 0, 1, 1, '2019-04-21 19:24:22'),
(126, 1, 125, 1, 0, 1, 1, '2019-04-21 19:24:22'),
(127, 1, 126, 1, 0, 1, 1, '2019-04-21 19:24:22'),
(128, 1, 127, 1, 0, 1, 1, '2019-04-21 19:24:22'),
(129, 1, 128, 1, 0, 1, 1, '2019-04-21 19:24:22'),
(130, 1, 129, 1, 0, 1, 1, '2019-04-21 19:25:54'),
(131, 1, 130, 1, 0, 1, 1, '2019-04-21 19:25:54'),
(132, 1, 131, 1, 0, 1, 1, '2019-04-21 19:25:54'),
(133, 1, 132, 1, 0, 1, 1, '2019-04-21 19:25:54'),
(134, 1, 133, 1, 0, 1, 1, '2019-04-21 19:25:54'),
(135, 1, 134, 1, 0, 1, 1, '2019-04-21 19:25:54'),
(136, 1, 135, 1, 0, 1, 1, '2019-04-21 19:25:54'),
(137, 1, 136, 1, 0, 1, 1, '2019-04-21 19:25:55'),
(138, 2, 137, 1, 0, 1, 1, '2019-04-22 21:55:03'),
(139, 2, 138, 1, 0, 1, 1, '2019-04-22 21:55:26'),
(140, 2, 139, 1, 0, 1, 1, '2019-04-22 22:05:06'),
(141, 2, 140, 1, 0, 1, 1, '2019-04-22 22:05:06'),
(142, 2, 141, 1, 0, 1, 1, '2019-04-22 22:41:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_icons`
--

CREATE TABLE IF NOT EXISTS `tbl_icons` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_icons`
--

INSERT INTO `tbl_icons` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'fa-500px', 'description', 1, 1, '2018-02-14 22:06:55'),
(2, 'fa-battery-1', '-', 1, 1, '2018-02-14 22:09:37'),
(3, 'fa-battery-empty', '-', 1, 1, '2018-02-14 22:10:42'),
(4, 'fa-battery-three-quarters', '-', 1, 1, '2018-02-14 22:13:07'),
(5, 'fa-calendar-plus-o', '-', 1, 1, '2018-02-14 22:13:35'),
(6, 'fa-chrome', '-', 1, 1, '2018-02-14 22:16:45'),
(7, 'fa-contao', '-', 1, 1, '2018-02-14 22:18:56'),
(8, 'fa-fonticons', '-', 1, 1, '2018-02-14 22:19:40'),
(9, 'fa-gg-circle', '-', 1, 1, '2018-02-14 22:20:30'),
(10, 'fa-hand-peace-o', '-', 1, 1, '2018-02-15 09:17:46'),
(11, 'fa-hand-spock-o', '-', 1, 1, '2018-02-15 09:18:34'),
(12, 'fa-hourglass-2', '-', 1, 1, '2018-02-15 09:19:37'),
(13, 'fa-hourglass-o', '-', 1, 1, '2018-02-17 00:31:49'),
(14, 'fa-industry', '-', 1, 1, '2018-02-18 20:15:10'),
(15, 'fa-map-pin', '-', 1, 1, '2018-02-18 20:17:28'),
(16, 'fa-object-ungroup', '-', 1, 1, '2018-02-18 20:19:00'),
(17, 'fa-opera', '-', 1, 1, '2018-02-18 20:19:53'),
(18, 'fa-sticky-note', '-', 1, 1, '2018-02-18 20:20:32');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_layouts`
--

CREATE TABLE IF NOT EXISTS `tbl_layouts` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_layouts`
--

INSERT INTO `tbl_layouts` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'login.min.css', '-', 1, 1, '2019-03-28 00:00:00'),
(2, 'login-2.min.css', '-', 1, 1, '2019-03-28 00:00:00'),
(3, 'login-3.min.css', '-', 1, 1, '2019-03-28 00:00:00'),
(4, 'login-4.min.css', '-', 1, 1, '2019-03-28 00:00:00'),
(5, 'login-5.min.css', '-', 1, 1, '2019-03-28 00:00:00'),
(6, 'login-5.min.css', '-', 1, 1, '2019-03-28 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_mahasiswa`
--

CREATE TABLE IF NOT EXISTS `tbl_mahasiswa` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `npm` int(16) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jenis_kelamin` tinyint(4) NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(16) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tahun_masuk` date NOT NULL,
  `semester` int(1) NOT NULL,
  `kelas` varchar(2) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '0',
  `creat_by` int(32) NOT NULL,
  `creat_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_mata_kuliah`
--

CREATE TABLE IF NOT EXISTS `tbl_mata_kuliah` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `name_matkul` varchar(50) NOT NULL,
  `descriptions` varchar(255) NOT NULL,
  `created_by` int(32) NOT NULL,
  `created_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_matkul_dosen`
--

CREATE TABLE IF NOT EXISTS `tbl_matkul_dosen` (
  `id_mata_kuliah` int(32) NOT NULL,
  `id_dosen` int(32) NOT NULL,
  `descriptions` varchar(255) NOT NULL,
  `created_by` int(32) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_menus`
--

CREATE TABLE IF NOT EXISTS `tbl_menus` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `rank` tinyint(1) NOT NULL DEFAULT '0',
  `level` tinyint(1) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `module_id` int(32) DEFAULT NULL,
  `is_logged_in` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `parent_id` int(32) NOT NULL,
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=93 ;

--
-- Dumping data untuk tabel `tbl_menus`
--

INSERT INTO `tbl_menus` (`id`, `name`, `path`, `rank`, `level`, `icon`, `module_id`, `is_logged_in`, `is_active`, `description`, `parent_id`, `created_by`, `create_date`) VALUES
(1, 'Ticket', '#', 0, 1, '', 1, 1, 0, '-', 0, 1, '2019-03-28 00:00:00'),
(2, 'Report', '#', 0, 1, '', 1, 1, 1, '-', 0, 1, '2019-03-28 00:00:00'),
(3, 'Master Data', '#', 0, 1, '', 0, 1, 1, '-', 0, 1, '2019-03-28 00:00:00'),
(4, 'Settings', '#', 0, 1, '', 0, 1, 1, '-', 0, 1, '2019-03-28 00:00:00'),
(5, 'Create', 'ticket/create', 0, 2, '', 1, 1, 1, '-', 1, 1, '2019-03-28 00:00:00'),
(6, 'Ticket Status', '#', 0, 2, '', 1, 1, 1, '', 1, 1, '2019-03-28 00:00:00'),
(7, 'View', 'report/view', 0, 2, '', 1, 1, 1, '-', 2, 1, '2019-03-28 00:00:00'),
(8, 'First Category', '#', 0, 2, '2', 1, 1, 1, '-', 8, 1, '2019-03-28 00:00:00'),
(9, 'Status', 'master/status', 0, 2, '', 1, 1, 1, '-', 3, 1, '2019-03-28 00:00:00'),
(10, 'Vendor', 'master/vendor', 0, 2, '', 1, 1, 1, '-', 3, 1, '2019-03-28 00:00:00'),
(11, 'Priority', 'master/priority', 0, 2, '', 1, 1, 1, '-', 3, 1, '2019-03-28 00:00:00'),
(12, 'Open', 'ticket/view/open', 0, 3, '', 1, 1, 1, '-', 6, 1, '2019-03-28 00:00:00'),
(13, 'Progress', 'ticket/view/progress', 0, 3, '', 1, 1, 1, '-', 6, 1, '2019-03-28 00:00:00'),
(14, 'Transfer', 'ticket/view/transfer', 0, 3, '', 1, 1, 1, '-', 6, 1, '2019-03-28 00:00:00'),
(15, 'Close', 'ticket/view/close', 0, 3, '', 1, 1, 1, '-', 6, 1, '2019-03-28 00:00:00'),
(41, 'Ticket', '#', 0, 1, '4', 2, 1, 0, '-', 0, 1, '2019-04-09 09:57:21'),
(42, 'View', '#', 0, 2, '3', 2, 1, 1, '-', 41, 1, '2019-04-09 09:57:56'),
(43, 'Open', 'ticket/master/view/open', 0, 3, '8', 2, 1, 1, '', 42, 1, '2019-04-09 09:58:18'),
(44, 'Progress', 'ticket/master/view/progress', 0, 3, '3', 2, 1, 1, '', 42, 1, '2019-04-09 09:58:39'),
(45, 'Close', 'ticket/master/view/close', 0, 3, '6', 2, 1, 1, '', 42, 1, '2019-04-09 09:59:01'),
(46, 'Transfer', 'ticket/master/view/transfer', 0, 3, '4', 2, 1, 1, '', 42, 1, '2019-04-09 09:59:56'),
(47, 'categories', 'ticket/category/view', 0, 3, '6', 2, 1, 1, '', 58, 1, '2019-04-09 10:00:13'),
(48, 'Master Data', '#', 0, 1, '10', 2, 1, 1, '-', 0, 1, '2019-04-09 10:00:54'),
(49, 'User', 'master/user/view', 0, 2, '5', 2, 1, 1, '', 48, 1, '2019-04-09 10:01:13'),
(50, 'Group', 'master/group/view', 0, 2, '3', 2, 1, 1, '', 48, 1, '2019-04-09 10:02:07'),
(51, 'Blog', '#', 0, 1, '4', 2, 1, 1, '-', 0, 1, '2019-04-09 10:03:54'),
(52, 'Create', 'app/content/add_content', 0, 2, '7', 2, 1, 1, '', 51, 1, '2019-04-09 10:04:37'),
(53, 'View', 'app/content/view', 0, 2, '7', 2, 1, 1, '', 51, 1, '2019-04-09 10:04:57'),
(54, 'Categories', 'app/category/view', 0, 2, '7', 2, 1, 1, '', 51, 1, '2019-04-09 10:05:15'),
(55, 'Prefferences', '#', 0, 1, '3', 2, 1, 1, '-', 0, 1, '2019-04-09 10:05:55'),
(56, 'Permission', 'master/permission/view', 0, 2, '2', 2, 1, 1, '', 48, 1, '2019-04-09 10:06:16'),
(57, 'Status', 'ticket/status/view', 0, 3, '8', 2, 1, 1, '', 58, 1, '2019-04-09 10:06:58'),
(58, 'Options', '#', 0, 2, '3', 2, 1, 1, '-', 41, 1, '2019-04-09 10:07:20'),
(59, 'Vendor', 'master/vendor/view', 0, 2, '3', 2, 1, 1, '', 48, 1, '2019-04-09 10:11:05'),
(60, 'Branch Office', 'master/branch/view', 0, 2, '9', 2, 1, 1, '', 48, 1, '2019-04-09 10:11:50'),
(61, 'Priorities', 'ticket/priority/view', 0, 3, '4', 2, 1, 1, '', 58, 1, '2019-04-09 10:12:37'),
(62, 'Rules', 'ticket/rules/view', 0, 3, '10', 2, 1, 1, '', 58, 1, '2019-04-09 10:13:09'),
(63, 'Employees', 'master/employee/view', 0, 2, '5', 2, 1, 1, '', 48, 1, '2019-04-09 10:13:55'),
(64, 'Config', 'settings/config/view', 0, 2, '10', 2, 1, 1, '', 55, 1, '2019-04-09 10:16:49'),
(65, 'Menu', 'settings/menu/view', 0, 2, '12', 2, 1, 1, '-', 55, 1, '2019-04-09 10:17:45'),
(66, 'Tools', '#', 0, 2, '8', 2, 1, 1, '-', 48, 1, '2019-04-09 10:18:45'),
(67, 'Icons', 'master/icon/view', 0, 3, '5', 2, 1, 1, '', 66, 1, '2019-04-09 10:19:08'),
(68, 'Modules', 'master/module/view', 0, 3, '6', 2, 1, 1, '', 66, 1, '2019-04-09 10:19:51'),
(69, 'Method', 'master/method/view', 0, 3, '18', 2, 1, 1, '', 66, 1, '2019-04-09 10:20:30'),
(70, 'Layouts', 'master/layout/view', 0, 3, '14', 2, 1, 1, '', 66, 1, '2019-04-09 10:22:17'),
(71, 'Issue Suggestion', 'ticket/issue_suggestion/view', 0, 3, '10', 2, 1, 1, '', 58, 1, '2019-04-09 10:23:01'),
(72, 'Group User', 'settings/user_group/view', 0, 2, '3', 2, 0, 0, '', 55, 1, '2019-04-09 10:56:09'),
(73, 'User Group', 'settings/user_group/view', 0, 2, '4', 2, 1, 1, '-', 55, 1, '2019-04-09 10:57:31'),
(74, 'Group Permission', 'settings/group_permission/view', 0, 2, '6', 2, 1, 1, '', 55, 1, '2019-04-09 10:58:14'),
(75, 'Comments', 'app/comment/view', 0, 2, '2', 2, 1, 1, '', 51, 1, '2019-04-09 14:50:19'),
(76, 'Comments', 'app/comment/view', 0, 2, '-- select one --', 2, 0, 0, '', 51, 1, '2019-04-09 14:50:59'),
(77, 'Ticket', '#', 0, 1, '10', 3, 1, 0, '-', 0, 1, '2019-04-10 05:04:05'),
(78, 'Report', '#', 0, 1, '8', 3, 1, 1, '-', 0, 1, '2019-04-10 05:04:41'),
(79, 'Open', 'vendor/ticket/view/open', 0, 2, '9', 3, 1, 1, '', 77, 1, '2019-04-10 05:05:14'),
(80, 'Progress', 'vendor/ticket/view/progress', 0, 2, '16', 3, 1, 1, '', 77, 1, '2019-04-10 05:05:43'),
(81, 'Transfer', 'vendor/ticket/view/transfer', 0, 2, '12', 3, 1, 1, '', 77, 1, '2019-04-10 05:06:10'),
(82, 'Close', 'vendor/ticket/view/close', 0, 2, '12', 3, 1, 1, '-', 77, 1, '2019-04-10 05:06:26'),
(83, 'View', '#', 0, 2, '6', 3, 1, 1, '-', 78, 1, '2019-04-10 05:06:54'),
(84, 'Profile', 'my-profile', 0, 1, '11', 1, 1, 1, '', 0, 1, '2019-04-10 05:07:24'),
(85, 'Lock Screen', 'lock-screen', 0, 1, '8', 1, 1, 1, '-', 0, 1, '2019-04-10 05:08:24'),
(86, 'Logout', 'logout', 0, 1, '11', 1, 1, 1, '', 0, 1, '2019-04-10 05:08:45'),
(87, 'Profile', 'vendor/my-profile', 0, 1, '5', 3, 1, 1, '', 0, 1, '2019-04-10 13:14:57'),
(88, 'Lock Screen', 'vendor/lock-screen', 0, 1, '11', 3, 1, 1, '-', 0, 1, '2019-04-10 13:16:04'),
(89, 'Logout', 'vendor/logout', 0, 1, '10', 3, 1, 1, '-', 0, 1, '2019-04-10 13:16:36'),
(90, 'Vendor contract', 'ticket/vendor_contract/view', 0, 3, '6', 2, 1, 1, '', 58, 1, '2019-04-21 18:38:36'),
(91, 'Options', '#', 0, 2, '6', 3, 1, 1, '-', 77, 1, '2019-04-22 09:06:33'),
(92, 'Issue Suggestion', 'vendor/issue_suggestion/view', 0, 3, '8', 3, 1, 1, '', 91, 1, '2019-04-22 09:08:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_method_masters`
--

CREATE TABLE IF NOT EXISTS `tbl_method_masters` (
  `id` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `rank` int(2) NOT NULL,
  `is_mandatory` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_method_masters`
--

INSERT INTO `tbl_method_masters` (`id`, `name`, `description`, `rank`, `is_mandatory`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'index', '-', 1, 1, 1, 1, '2018-10-17 00:00:00'),
(2, 'view', '-', 2, 1, 1, 2, '2018-10-17 00:00:00'),
(3, 'insert', '-', 3, 1, 1, 1, '2018-10-17 00:00:00'),
(4, 'remove', '-', 4, 1, 1, 2, '2018-10-17 00:00:00'),
(5, 'delete', '-', 5, 1, 1, 1, '2018-10-17 00:00:00'),
(6, 'dashboard', '-', 6, 0, 1, 1, '2018-10-17 21:39:30'),
(7, 'logout', '-', 7, 0, 1, 1, '2018-10-17 21:40:44'),
(8, 'login', '-', 8, 0, 1, 1, '2018-10-17 21:40:44'),
(9, 'auth', '-', 9, 0, 1, 1, '2018-10-17 21:40:02'),
(10, 'update', '-', 10, 1, 1, 1, '2018-10-17 00:00:00'),
(11, 'update_status', '-', 11, 0, 1, 1, '2018-10-17 00:00:00'),
(12, 'get_list', '-', 12, 1, 1, 1, '2018-10-17 00:00:00'),
(13, 'get_data', '-', 13, 1, 1, 1, '2018-10-17 00:00:00'),
(14, 'get_group', '-', 14, 0, 1, 1, '2018-10-17 00:00:00'),
(15, 'get_method', '-', 15, 0, 1, 1, '2018-10-17 00:00:00'),
(16, 'get_icon', '-', 16, 0, 1, 1, '2018-10-26 09:27:35'),
(17, 'get_menu', '-', 17, 0, 1, 1, '2018-10-29 19:57:39'),
(18, 'task', '-', 18, 0, 1, 1, '2018-11-20 21:27:47'),
(19, 'inbox', '-', 19, 0, 1, 1, '2018-11-20 21:27:58'),
(20, 'rank', '-', 20, 0, 1, 1, '2018-11-24 15:53:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_modules`
--

CREATE TABLE IF NOT EXISTS `tbl_modules` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `tbl_modules`
--

INSERT INTO `tbl_modules` (`id`, `name`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'frontend', '-', 1, 1, '2019-01-18 00:00:00'),
(2, 'backend', '-', 1, 1, '2019-01-18 00:00:00'),
(3, 'lecturer', '-', 1, 1, '2019-01-18 00:00:00'),
(4, 'api', '-', 0, 1, '2019-01-18 00:00:00'),
(5, 'developer', '-', 0, 1, '2019-01-18 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_permissions`
--

CREATE TABLE IF NOT EXISTS `tbl_permissions` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `module` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=142 ;

--
-- Dumping data untuk tabel `tbl_permissions`
--

INSERT INTO `tbl_permissions` (`id`, `module`, `class`, `action`, `description`, `is_active`, `created_by`, `create_date`) VALUES
(1, 'backend', 'user', 'login', '-', 1, 1, '0000-00-00 00:00:00'),
(2, 'backend', 'user', 'logout', '-', 1, 1, '0000-00-00 00:00:00'),
(3, 'backend', 'user', 'dashboard', '-', 1, 1, '0000-00-00 00:00:00'),
(4, 'backend', 'user', 'my_profile', '-', 1, 1, '0000-00-00 00:00:00'),
(5, 'backend', 'user', 'my_inbox', '-', 1, 1, '0000-00-00 00:00:00'),
(6, 'backend', 'user', 'my_task', '-', 1, 1, '0000-00-00 00:00:00'),
(7, 'backend', 'user', 'lock_screen', '-', 1, 1, '0000-00-00 00:00:00'),
(8, 'backend', 'user', 'get_user_profile', '-', 1, 1, '0000-00-00 00:00:00'),
(9, 'backend', 'user', 'index', '-', 1, 1, '0000-00-00 00:00:00'),
(10, 'backend', 'user', 'view', '-', 1, 1, '0000-00-00 00:00:00'),
(11, 'backend', 'user', 'get_list', '-', 1, 1, '0000-00-00 00:00:00'),
(12, 'backend', 'user', 'get_data', '-', 1, 1, '0000-00-00 00:00:00'),
(13, 'backend', 'user', 'insert', '-', 1, 1, '0000-00-00 00:00:00'),
(14, 'backend', 'user', 'update', '-', 1, 1, '0000-00-00 00:00:00'),
(15, 'backend', 'user', 'update_status', '-', 1, 1, '0000-00-00 00:00:00'),
(16, 'backend', 'user', 'remove', '-', 1, 1, '0000-00-00 00:00:00'),
(17, 'backend', 'user', 'delete', '-', 1, 1, '0000-00-00 00:00:00'),
(18, 'backend', 'group', 'index', '-', 1, 1, '0000-00-00 00:00:00'),
(19, 'backend', 'group', 'view', '-', 1, 1, '0000-00-00 00:00:00'),
(20, 'backend', 'group', 'get_list', '-', 1, 1, '0000-00-00 00:00:00'),
(21, 'backend', 'group', 'get_data', '-', 1, 1, '0000-00-00 00:00:00'),
(22, 'backend', 'group', 'insert', '-', 1, 1, '0000-00-00 00:00:00'),
(23, 'backend', 'group', 'update', '-', 1, 1, '0000-00-00 00:00:00'),
(24, 'backend', 'group', 'update_status', '-', 1, 1, '0000-00-00 00:00:00'),
(25, 'backend', 'group', 'remove', '-', 1, 1, '0000-00-00 00:00:00'),
(26, 'backend', 'group', 'delete', '-', 1, 1, '0000-00-00 00:00:00'),
(27, 'backend', 'group_permission', 'index', '-', 1, 1, '0000-00-00 00:00:00'),
(28, 'backend', 'group_permission', 'view', '-', 1, 1, '0000-00-00 00:00:00'),
(29, 'backend', 'group_permission', 'get_list', '-', 1, 1, '0000-00-00 00:00:00'),
(30, 'backend', 'group_permission', 'get_data', '-', 1, 1, '0000-00-00 00:00:00'),
(31, 'backend', 'group_permission', 'insert', '-', 1, 1, '0000-00-00 00:00:00'),
(32, 'backend', 'group_permission', 'update', '-', 1, 1, '0000-00-00 00:00:00'),
(33, 'backend', 'group_permission', 'update_status', '-', 1, 1, '0000-00-00 00:00:00'),
(34, 'backend', 'group_permission', 'remove', '-', 1, 1, '0000-00-00 00:00:00'),
(35, 'backend', 'group_permission', 'delete', '-', 1, 1, '0000-00-00 00:00:00'),
(36, 'backend', 'group_permission', 'get_group', '-', 1, 1, '0000-00-00 00:00:00'),
(37, 'backend', 'group_permission', 'get_method', '-', 1, 1, '0000-00-00 00:00:00'),
(38, 'backend', 'group_permission', 'get_module', '-', 1, 1, '0000-00-00 00:00:00'),
(39, 'backend', 'email_layout', 'index', '-', 1, 1, '0000-00-00 00:00:00'),
(40, 'backend', 'email_layout', 'view', '-', 1, 1, '0000-00-00 00:00:00'),
(41, 'backend', 'email_layout', 'insert', '-', 1, 1, '0000-00-00 00:00:00'),
(42, 'backend', 'email_layout', 'remove', '-', 1, 1, '0000-00-00 00:00:00'),
(43, 'backend', 'email_layout', 'delete', '-', 1, 1, '0000-00-00 00:00:00'),
(44, 'backend', 'email_layout', 'update', '-', 1, 1, '0000-00-00 00:00:00'),
(45, 'backend', 'email_layout', 'get_list', '-', 1, 1, '0000-00-00 00:00:00'),
(46, 'backend', 'email_layout', 'get_data', '-', 1, 1, '0000-00-00 00:00:00'),
(47, 'backend', 'menu', 'index', '-', 1, 1, '0000-00-00 00:00:00'),
(48, 'backend', 'menu', 'view', '-', 1, 1, '0000-00-00 00:00:00'),
(49, 'backend', 'menu', 'get_all_data', '-', 1, 1, '0000-00-00 00:00:00'),
(50, 'backend', 'menu', 'get_data', '-', 1, 1, '0000-00-00 00:00:00'),
(51, 'backend', 'menu', 'get_controller_prefixs', '-', 1, 1, '0000-00-00 00:00:00'),
(52, 'backend', 'menu', 'get_icon', '-', 1, 1, '0000-00-00 00:00:00'),
(53, 'backend', 'menu', 'get_menu', '-', 1, 1, '0000-00-00 00:00:00'),
(54, 'backend', 'menu', 'insert', '-', 1, 1, '0000-00-00 00:00:00'),
(55, 'backend', 'menu', 'update', '-', 1, 1, '0000-00-00 00:00:00'),
(56, 'backend', 'menu', 'update_status', '-', 1, 1, '0000-00-00 00:00:00'),
(57, 'backend', 'menu', 'remove', '-', 1, 1, '0000-00-00 00:00:00'),
(58, 'backend', 'menu', 'delete', '-', 1, 1, '0000-00-00 00:00:00'),
(59, 'backend', 'menu', 'get_module', '-', 1, 1, '0000-00-00 00:00:00'),
(60, 'backend', 'menu', 'get_parent_detail', '-', 1, 1, '0000-00-00 00:00:00'),
(61, 'backend', 'user_group', 'index', '-', 1, 1, '0000-00-00 00:00:00'),
(62, 'backend', 'user_group', 'view', '-', 1, 1, '0000-00-00 00:00:00'),
(63, 'backend', 'user_group', 'get_list', '-', 1, 1, '0000-00-00 00:00:00'),
(64, 'backend', 'user_group', 'get_data', '-', 1, 1, '0000-00-00 00:00:00'),
(65, 'backend', 'user_group', 'insert', '-', 1, 1, '0000-00-00 00:00:00'),
(66, 'backend', 'user_group', 'update', '-', 1, 1, '0000-00-00 00:00:00'),
(67, 'backend', 'user_group', 'update_status', '-', 1, 1, '0000-00-00 00:00:00'),
(68, 'backend', 'user_group', 'remove', '-', 1, 1, '0000-00-00 00:00:00'),
(69, 'backend', 'user_group', 'delete', '-', 1, 1, '0000-00-00 00:00:00'),
(70, 'backend', 'permission', 'index', '-', 1, 1, '2019-04-16 15:55:12'),
(71, 'backend', 'permission', 'view', '-', 1, 1, '2019-04-16 15:55:12'),
(72, 'backend', 'permission', 'insert', '-', 1, 1, '2019-04-16 15:55:12'),
(73, 'backend', 'permission', 'remove', '-', 1, 1, '2019-04-16 15:55:12'),
(74, 'backend', 'permission', 'delete', '-', 1, 1, '2019-04-16 15:55:13'),
(75, 'backend', 'permission', 'update', '-', 1, 1, '2019-04-16 15:55:13'),
(76, 'backend', 'permission', 'get_list', '-', 1, 1, '2019-04-16 15:55:13'),
(77, 'backend', 'permission', 'get_data', '-', 1, 1, '2019-04-16 15:55:13'),
(78, 'frontend', 'user', 'dashboard', '-', 1, 1, '2019-04-17 19:36:48'),
(79, 'frontend', 'user', 'my_profile', '-', 1, 1, '2019-04-17 19:38:30'),
(80, 'frontend', 'user', 'my_inbox', '-', 1, 1, '2019-04-17 19:39:03'),
(81, 'frontend', 'user', 'my_task', '', 1, 1, '2019-04-17 19:39:17'),
(82, 'frontend', 'user', 'my_notif', '', 1, 1, '2019-04-17 19:39:28'),
(83, 'frontend', 'user', 'lock_screen', '', 1, 1, '2019-04-17 19:39:41'),
(84, 'frontend', 'user', 'un_lock_screen', '', 1, 1, '2019-04-17 19:39:56'),
(85, 'frontend', 'user', 'switch_lang', '', 1, 1, '2019-04-17 19:40:12'),
(86, 'frontend', 'ticket', 'action', '', 1, 1, '2019-04-17 19:40:58'),
(87, 'frontend', 'ticket', 'create', '', 1, 1, '2019-04-17 19:41:15'),
(88, 'frontend', 'ticket', 'get_category', '', 1, 1, '2019-04-17 19:41:30'),
(89, 'api', 'ticket', 'get_vendor', '', 1, 1, '2019-04-17 19:41:53'),
(90, 'frontend', 'ticket', 'view', '', 1, 1, '2019-04-17 19:42:24'),
(91, 'frontend', 'ticket', 'get_list', '', 1, 1, '2019-04-17 19:42:35'),
(92, 'frontend', 'user', 'get_all_history', '', 1, 1, '2019-04-17 19:53:27'),
(93, 'frontend', 'ticket', 'insert', '-', 1, 1, '2019-04-17 21:45:17'),
(94, 'frontend', 'ticket', 'tracking', '-', 1, 1, '2019-04-18 15:57:32'),
(95, 'frontend', 'user', 'index', '-', 1, 1, '2019-04-20 20:33:31'),
(96, 'frontend', 'user', 'view', '-', 1, 1, '2019-04-20 20:33:31'),
(97, 'frontend', 'user', 'insert', '-', 1, 1, '2019-04-20 20:33:31'),
(98, 'frontend', 'user', 'remove', '-', 1, 1, '2019-04-20 20:33:31'),
(99, 'frontend', 'user', 'delete', '-', 1, 1, '2019-04-20 20:33:32'),
(100, 'frontend', 'user', 'update', '-', 1, 1, '2019-04-20 20:33:32'),
(101, 'frontend', 'user', 'get_list', '-', 1, 1, '2019-04-20 20:33:32'),
(102, 'frontend', 'user', 'get_data', '-', 1, 1, '2019-04-20 20:33:32'),
(103, 'frontend', 'user', 'dashboard', '-', 1, 1, '2019-04-20 20:33:32'),
(104, 'frontend', 'user', 'logout', '-', 1, 1, '2019-04-20 20:33:32'),
(105, 'frontend', 'user', 'login', '-', 1, 1, '2019-04-20 20:33:32'),
(106, 'frontend', 'user', 'auth', '-', 1, 1, '2019-04-20 20:33:32'),
(107, 'frontend', 'user', 'update_status', '-', 1, 1, '2019-04-20 20:33:32'),
(108, 'developer', 'user', 'index', '-', 1, 1, '2019-04-21 09:02:57'),
(109, 'developer', 'user', 'view', '-', 1, 1, '2019-04-21 09:02:57'),
(110, 'developer', 'user', 'insert', '-', 1, 1, '2019-04-21 09:02:57'),
(111, 'developer', 'user', 'remove', '-', 1, 1, '2019-04-21 09:02:57'),
(112, 'developer', 'user', 'delete', '-', 1, 1, '2019-04-21 09:02:57'),
(113, 'developer', 'user', 'update', '-', 1, 1, '2019-04-21 09:02:57'),
(114, 'developer', 'user', 'get_list', '-', 1, 1, '2019-04-21 09:02:57'),
(115, 'developer', 'user', 'get_data', '-', 1, 1, '2019-04-21 09:02:57'),
(116, 'developer', 'user', 'dashboard', '-', 1, 1, '2019-04-21 09:02:57'),
(117, 'developer', 'user', 'logout', '-', 1, 1, '2019-04-21 09:02:58'),
(118, 'developer', 'user', 'login', '-', 1, 1, '2019-04-21 09:02:58'),
(119, 'developer', 'user', 'auth', '-', 1, 1, '2019-04-21 09:02:58'),
(120, 'developer', 'user', 'update_status', '-', 1, 1, '2019-04-21 09:02:58'),
(121, 'backend', 'Vendor_contract', 'index', '-', 1, 1, '2019-04-21 19:24:21'),
(122, 'backend', 'Vendor_contract', 'view', '-', 1, 1, '2019-04-21 19:24:21'),
(123, 'backend', 'Vendor_contract', 'insert', '-', 1, 1, '2019-04-21 19:24:21'),
(124, 'backend', 'Vendor_contract', 'remove', '-', 1, 1, '2019-04-21 19:24:21'),
(125, 'backend', 'Vendor_contract', 'delete', '-', 1, 1, '2019-04-21 19:24:22'),
(126, 'backend', 'Vendor_contract', 'update', '-', 1, 1, '2019-04-21 19:24:22'),
(127, 'backend', 'Vendor_contract', 'get_list', '-', 1, 1, '2019-04-21 19:24:22'),
(128, 'backend', 'Vendor_contract', 'get_data', '-', 1, 1, '2019-04-21 19:24:22'),
(129, 'backend', 'Issue_suggestion', 'index', '-', 1, 1, '2019-04-21 19:25:54'),
(130, 'backend', 'Issue_suggestion', 'view', '-', 1, 1, '2019-04-21 19:25:54'),
(131, 'backend', 'Issue_suggestion', 'insert', '-', 1, 1, '2019-04-21 19:25:54'),
(132, 'backend', 'Issue_suggestion', 'remove', '-', 1, 1, '2019-04-21 19:25:54'),
(133, 'backend', 'Issue_suggestion', 'delete', '-', 1, 1, '2019-04-21 19:25:54'),
(134, 'backend', 'Issue_suggestion', 'update', '-', 1, 1, '2019-04-21 19:25:54'),
(135, 'backend', 'Issue_suggestion', 'get_list', '-', 1, 1, '2019-04-21 19:25:54'),
(136, 'backend', 'Issue_suggestion', 'get_data', '-', 1, 1, '2019-04-21 19:25:55'),
(137, 'frontend', 'ticket', 'tracking', '-', 1, 1, '2019-04-22 21:55:03'),
(138, 'frontend', 'ticket', 'get_content', '-', 1, 1, '2019-04-22 21:55:26'),
(139, 'frontend', 'ticket', 'insert_message', '-', 1, 1, '2019-04-22 22:05:06'),
(140, 'frontend', 'ticket', 'insert_message', '-', 1, 1, '2019-04-22 22:05:06'),
(141, 'frontend', 'ticket', 'get_ticket_detail', '-', 1, 1, '2019-04-22 22:41:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_questions`
--

CREATE TABLE IF NOT EXISTS `tbl_questions` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `value` text NOT NULL,
  `id_type` int(32) NOT NULL,
  `descriptions` varchar(255) NOT NULL,
  `created_by` int(32) NOT NULL,
  `created_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_question_types`
--

CREATE TABLE IF NOT EXISTS `tbl_question_types` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `descriptions` varchar(255) NOT NULL,
  `created_by` int(32) NOT NULL,
  `created_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_result`
--

CREATE TABLE IF NOT EXISTS `tbl_result` (
  `id` int(32) NOT NULL,
  `total_questions` int(40) NOT NULL,
  `id_type` int(32) NOT NULL,
  `correct_answer` int(2) NOT NULL,
  `final_score` int(3) NOT NULL,
  `created_by` int(32) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(155) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(128) NOT NULL,
  `activation_code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 => present, 2 => away, 3 => inactive',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_logged_in` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `username`, `first_name`, `last_name`, `email`, `password`, `activation_code`, `status`, `is_active`, `is_logged_in`, `created_by`, `create_date`) VALUES
(1, 'arif', 'arif', 'firman syah', 'firman.begin@gmail.com', '$2y$12$tVM850A1gUWQjh/eR6EV6.Sy.L6vOxBpssQdiQrkvBZJ.MCx5o4By', '', 1, 1, 1, 1, '2019-01-16 00:00:00'),
(2, 'user', 'user', 'oreno', 'admin@admin.com', '$2y$12$tVM850A1gUWQjh/eR6EV6.Sy.L6vOxBpssQdiQrkvBZJ.MCx5o4By', '', 1, 1, 0, 1, '2019-01-16 00:00:00'),
(3, 'dadang', 'dadang', 'dosen', 'dadang@gmail.com', '$2y$12$tVM850A1gUWQjh/eR6EV6.Sy.L6vOxBpssQdiQrkvBZJ.MCx5o4By', 'QMyxHjcl1duBmNR853ZwasPLITl6io1e', 3, 1, 1, 1, '2019-03-11 09:04:06'),
(4, 'irama', 'rama', 'mahasiswa', 'rama@gmail.com', '$2y$12$tVM850A1gUWQjh/eR6EV6.Sy.L6vOxBpssQdiQrkvBZJ.MCx5o4By', 'QMyxHjcl1duBmNR853ZwasPLITl6io1e', 3, 1, 1, 1, '2019-03-11 09:04:06'),
(5, 'vendor', 'vendor', 'mahasiswa', 'vendor@gmail.com', '$2y$12$tVM850A1gUWQjh/eR6EV6.Sy.L6vOxBpssQdiQrkvBZJ.MCx5o4By', 'QMyxHjcl1duBmNR853ZwasPLITl6io1e', 3, 1, 1, 1, '2019-03-11 09:04:06');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user_answers`
--

CREATE TABLE IF NOT EXISTS `tbl_user_answers` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `id_questions` int(32) NOT NULL,
  `id_type` int(32) NOT NULL,
  `value` text NOT NULL,
  `created_by` int(32) NOT NULL,
  `created_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user_dosen`
--

CREATE TABLE IF NOT EXISTS `tbl_user_dosen` (
  `id_dosen` int(32) NOT NULL AUTO_INCREMENT,
  `id_user` int(32) NOT NULL,
  `created_by` int(32) NOT NULL,
  `created_date` date NOT NULL,
  PRIMARY KEY (`id_dosen`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user_groups`
--

CREATE TABLE IF NOT EXISTS `tbl_user_groups` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `user_id` int(32) NOT NULL,
  `group_id` int(32) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `tbl_user_groups`
--

INSERT INTO `tbl_user_groups` (`id`, `user_id`, `group_id`, `is_active`, `created_by`, `create_date`) VALUES
(1, 1, 1, 1, 1, '2019-04-16 00:00:00'),
(2, 3, 3, 1, 1, '2019-04-16 00:00:00'),
(3, 4, 2, 1, 1, '2019-04-16 00:00:00'),
(4, 5, 2, 1, 1, '2019-04-16 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user_profiles`
--

CREATE TABLE IF NOT EXISTS `tbl_user_profiles` (
  `id` int(32) NOT NULL,
  `user_id` int(32) NOT NULL,
  `address` text NOT NULL,
  `lat` varchar(64) NOT NULL,
  `lng` varchar(64) NOT NULL,
  `zoom` int(4) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `instagram` varchar(255) NOT NULL,
  `linkedin` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(32) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
